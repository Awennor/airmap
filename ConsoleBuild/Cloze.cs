﻿using pidroh.conceptmap.model;
using Pidroh.BaseUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleBuild
{
    public class ClozeQuestion
    {
        public string answer;
        public string[] dummies;
        public string prompt;

        static List<string> aux = new List<string>();
        public List<string> GetRandomizedItems()
        {
            aux.Clear();
            aux.Add(answer);
            aux.AddRange(dummies);
            aux.Shuffle();
            return aux;
        }
    }

    public class ClozeGenerationMap
    {
        private ConceptMapData map;

        public ClozeQuestion[] GetQuestions(ConceptMapData map)
        {
            this.map = map;
            ClozeQuestion[] questions = new ClozeQuestion[map.Propositions.Count*3];
            int index = 0;
            List<int> aux = new List<int>();
            Action<int, Proposition> CreateQuestion = (int which, Proposition p) => 
            {
                ClozeQuestion q = questions[index];
                q = new ClozeQuestion();
                questions[index] = q;
                q.dummies = new string[4];
                q.answer = GetFromProposition(which, p);
                string n1 = map.NodeNames[p.Node1];
                string n2 = map.NodeNames[p.Node2];
                string l = map.LinkNames[p.Link];

                if (which == 0) n1 = "______";
                if (which == 1) n2 = "______";
                if (which == 2) l = "______";

                q.prompt = string.Format("{0} {1} {2}", n1, l, n2);
                
                for (int i = 0; i < q.dummies.Length; i++)
                {
                    List<string> listToDraw = map.NodeNames;
                    if (which == 2) listToDraw = map.LinkNames;
                    aux.Clear();
                    for (int i2 = 0; i2 < listToDraw.Count; i2++)
                    {
                        if (listToDraw[i2] == q.answer) continue;
                        bool flag = false;
                        foreach (var item2 in q.dummies)
                        {
                            if (item2 == listToDraw[i2]) {
                                flag = true;
                                break;
                            }

                        }
                        if (flag) continue;
                        aux.Add(i2);
                    }

                    var randomIndex = RandomSupplier.RandomElement(aux);
                    q.dummies[i] = listToDraw[randomIndex];
                }
                index++;
            };
            foreach (var item in map.Propositions)
            {
                CreateQuestion(0,item);
                CreateQuestion(1, item);
                CreateQuestion(2, item);
            }

            return questions;
        }

        private string GetFromProposition(int which, Proposition p)
        {
            if (which == 0) return map.NodeNames[p.Node1];
            if (which == 1) return map.NodeNames[p.Node2];
            if (which == 2) return map.LinkNames[p.Link];
            return null;
        }
    }

    public class QuestionScheduler
    {
        List<int> questions = new List<int>();

        public void Initialize(int numberOfQuestions)
        {
            for (int i = 0; i < numberOfQuestions; i++)
            {
                questions.Add(i);

            }
            questions.Shuffle();
        }

        public void Answer(bool correctness)
        {
            int q = questions[0];
            questions.RemoveAt(0);
            if (!correctness)
            {
                questions.Insert(5, q);
            }

        }

        internal bool HasQuestion()
        {
            return questions.Count > 0;
        }

        internal int Next()
        {
            return questions[0];
        }
    }
}
