﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pidroh.BaseUtils
{
    static public class RandomSupplier
    {
        public static Func<float> Generate{ get; set; }

        public static int Range(int min, int max) {
            return (int) (Generate() * (max-min)+min);
        }

        public static T RandomElement<T>(T[] array)
        {
            return array[Range(0, array.Length)];
        }

        public static T RandomElement<T>(List<T> array)
        {
            return array[Range(0, array.Count)];
        }
    }

    static public class ExtensionMethods {
        private static Random rng = new Random();

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}
