﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;
using UnityEngine.UI;

public class NodeComp : MonoBehaviour {

    public HorizontalLayoutGroup layout;
    [SerializeField]
    private Text label;
    private bool nodeSelected;
    public UnityEvent OnNodeSelectStart, OnNodeUnSelect, OnNodePointerDown, OnNodePointerUp, OnNodePointerEnter, OnNodePointerExit;
    public RectOffset linkPadding;
    private int type;
    private Vector3 latestPos;

    public bool NodeSelected {
        get {
            return nodeSelected;
        }

        set {
            nodeSelected = value;
            //Debug.Log("Node Selec "+value);
            if (value) {
                OnNodeSelectStart.Invoke();
            } else {
                OnNodeUnSelect.Invoke();
            }
        }
    }

    public int Type {
        get {
            return type;
        }

        set {
            type = value;
        }
    }

    public Vector3 LatestPos_GraphSpace
    {
        get
        {
            return latestPos;
        }

        set
        {
            latestPos = value;
        }
    }

    public Text Label
    {
        get
        {
            return label;
        }

        set
        {
            label = value;
        }
    }

    public string NodeText { get {
            return label.text;
        } }

    internal NodeDataCustom NodeDataCustom { get; set; }

    public void PointerDown() {
        
        OnNodePointerDown.Invoke();
    }

    public void PointerExit() {
        
        OnNodePointerExit.Invoke();
    }

    public void PointerEnter() {
        
        OnNodePointerEnter.Invoke();
    }

    public void PointerUp() {
        //Debug.Log("POINTER UP");
        OnNodePointerUp.Invoke();
    }

    public void SetNodeType(int node) {
        this.Type = node;
        if (node == NodeDataCustom.NodeType_Link) {
            GetComponent<Animator>().SetFloat("Linklike", 1);
            layout.padding = linkPadding;
            var rectT = layout.GetComponent<RectTransform>();
            rectT.sizeDelta = new Vector2(130, 70);
            //label.fontSize = 21;
        }
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
