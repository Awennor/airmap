﻿using pidroh.conceptmap.model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirMapController : MonoBehaviour {

    public GraphTest graphTest;
    public AirMapModel model;
    public event Action<Proposition> OnPropositionMade;
    public event Action<int, bool> OnNodeSelectionStatusChange;

    // Use this for initialization
    void Start () {
        graphTest.OnConnected += GraphTest_OnConnected;
        graphTest.OnNodeSelectedChange += GraphTest_OnNodeSelectedChange;
        graphTest.OnDisconnected += GraphTest_OnDisconnected;
	}

    

    private void GraphTest_OnNodeSelectedChange(pidroh.conceptmap.airmap.view.ViewStructures.NodeSelectChange obj)
    {
        int nodeId = model.GetNodeId(obj.NodeName);
        if (OnNodeSelectionStatusChange != null) {
            OnNodeSelectionStatusChange(nodeId, obj.Selected);
        }
    }

    private void GraphTest_OnConnected(pidroh.conceptmap.airmap.view.ViewStructures.PropositionMade proposition)
    {
        int node1Id = model.GetNodeId(proposition.Node1Name);
        int node2Id = model.GetNodeId(proposition.Node2Name);
        int linkId = model.GetLinkId(proposition.LinkName);
        
        Proposition propositionData = new Proposition(node1Id, node2Id, linkId);
        model.Accessor.AddPropositionInfo(propositionData);
        if(OnPropositionMade !=null)
        {
            
            OnPropositionMade(propositionData);
        }
    }

    private void GraphTest_OnDisconnected(pidroh.conceptmap.airmap.view.ViewStructures.PropositionMade proposition)
    {
        Debug.Log(string.Format("Disconnecting node: L {0} N2 {1} N1 {2}", proposition.LinkName, proposition.Node1Name, proposition.Node2Name));
        bool removed = model.Accessor.RemoveOnePropositionByNames(proposition.LinkName, proposition.Node1Name, proposition.Node2Name);
        if (!removed) {
            Debug.LogError("Failed to remove the proposition from the model");
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
