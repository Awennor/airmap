﻿using Pidroh.UnityUtils.LogDataSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextReadLog : MonoBehaviour {

    public LogCapturerManager log;
    public TextReadController tRC;

    // Use this for initialization
    void Start()
    {
        TextReadController.TextReadDescriptor descriptor = tRC.Descriptor;
        if(descriptor != null)
            log.DirectLog("readstart", descriptor.title);

    }

    public void ReadDone() {
        TextReadController.TextReadDescriptor descriptor = tRC.Descriptor;
        if (descriptor != null)
            log.DirectLog("readdone", tRC.Descriptor.title);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
