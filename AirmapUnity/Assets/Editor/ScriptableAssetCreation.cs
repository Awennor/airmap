﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Pidroh.UnityUtils.Misc
{

    public class ScriptableAssetCreation : EditorWindow
    //ScriptableWizard 
    {

        public string assetName;
        bool testOnce;
        static string fieldN = "TextFieldScriptableObject";

        [MenuItem("Assets/Create/Add Scriptable Object", priority = 5)]
        // Use this for initialization
        static void Start()
        {
            ScriptableAssetCreation window = (ScriptableAssetCreation)EditorWindow.GetWindow(typeof(ScriptableAssetCreation));
            window.Show();
            window.testOnce = true;
            GUI.FocusControl(fieldN);
            //Debug.Log("What...");
            //ScriptableWizard.DisplayWizard<ScriptableAssetCreation>("Create ScriptableObject", "Create");
        }



        void OnGUI()
        {

            GUI.SetNextControlName(fieldN);
            assetName = EditorGUILayout.TextField("S. Asset Name:", assetName);



            if (GUILayout.Button("Create") || Event.current.keyCode == KeyCode.Return)
            {
                OnWizardCreate();
            }
            if (testOnce)
            {
                //Debug.Log("Blaaa");
                GUI.FocusControl(fieldN);
                testOnce = false;
            }
            //Debug.Log(GUI.GetNameOfFocusedControl());
        }

        void OnWizardCreate()
        {

            string assetFileName = Application.dataPath + "/" + this.assetName + ".cs";
            var words = assetName.Split('/');
            string assetSimpleName = words[words.Length - 1];

            Debug.Log("ASSET CREATED " + assetFileName);
            File.WriteAllText(assetFileName, "using UnityEngine;\n\n" +
                "[CreateAssetMenu(fileName = \"" + assetSimpleName + "\", menuName = \"ScriptableObjects /" + assetSimpleName + "\", order = 1)]\n" +
                "public class " + assetSimpleName + " : ScriptableObject {\n" +
                "\n}");
            AssetDatabase.Refresh();
            Close();
        }

        void OnWizardUpdate()
        {
            //helpString = "Set name of the scriptable asset script. You can also put a path before the name.";
        }


    }
}