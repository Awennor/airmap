﻿using Assets.Scripts.ConceptMap.model;
using pidroh.conceptmap.model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirMapModel : MonoBehaviour
{

    ConceptMapDataAccessor accessor = new ConceptMapDataAccessor(new ConceptMapData());
    private ConceptMapData targetMap;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetTargetMap(ConceptMapData map) {
        targetMap = map;
        accessor.Data.NodeNames.AddRange(map.NodeNames);
        accessor.Data.LinkNames.AddRange(map.LinkNames);
        
    }

    public ConceptMapData MapData
    {
        get
        {
            return Accessor.Data;
        }
    }

    internal ConceptMapDataAccessor Accessor
    {
        get
        {
            return accessor;
        }

        set
        {
            accessor = value;
        }
    }

    public ConceptMapData TargetMap
    {
        get
        {
            return targetMap;
        }


    }

    internal int GetNodeId(string node1)
    {
        return Accessor.GetNodeId(node1);
    }

    internal int GetLinkId(string link)
    {
        return Accessor.GetLinkId(link);
    }
}
