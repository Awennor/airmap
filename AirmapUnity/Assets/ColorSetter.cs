﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorSetter : MonoBehaviour {

    public Color[] colors;
    [SerializeField]
    private Image[] image;
    [SerializeField]
    private Text[] text;
    [SerializeField]
    int defaultColor;

    public void SetColorAndDefaultIt(int c) {
        SetColor(c);
        defaultColor = c;
        Debug.Log("DEFAULT COLOR SHIT");
    }

    public void BackToDefaultColor() {
        SetColor(defaultColor);
        Debug.Log("DEFAULT COLOR SHIT2");
    }

    public void SetColor(int c) {
        for (int i = 0; i < image.Length; i++) {
            image[i].color = colors[c];
        }
        for (int i = 0; i < text.Length; i++) {
            text[i].color = colors[c];
        }
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
