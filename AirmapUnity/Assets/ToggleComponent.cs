﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleComponent : MonoBehaviour {
    [SerializeField]
    private Text label;
    [SerializeField]
    private Toggle toggle;

    public event Action<ToggleComponent> OnCheckChange;

    public Text Label
    {
        get
        {
            return label;
        }

        set
        {
            label = value;
        }
    }

    public Toggle Toggle
    {
        get
        {
            return toggle;
        }

        set
        {
            toggle = value;
        }
    }

    // Use this for initialization
    void Start () {
        toggle.onValueChanged.AddListener(OnValueChange);
	}

    private void OnValueChange(bool arg0)
    {
        if(OnCheckChange != null)
            OnCheckChange(this);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
