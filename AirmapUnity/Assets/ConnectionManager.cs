﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectionManager : MonoBehaviour {
    [SerializeField]
    List<ConnectionStatement> connections;
    public ConnectionChoserUIManager uiManager;

    public List<ConnectionStatement> Connections
    {
        get
        {
            return connections;
        }

        set
        {
            connections = value;
        }
    }

    // Use this for initialization
    void Start () {
		uiManager.ShowConnections(Connections);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [Serializable]
    public class ConnectionStatement {
        [SerializeField]
        string label;
        [SerializeField]
        int amount = 1;

        public ConnectionStatement()
        {

        }

        public ConnectionStatement(string label, int amount)
        {
            this.label = label;
            this.amount = amount;
        }

        public string Label {
            get {
                return label;
            }

            set {
                label = value;
            }
        }

        public int Amount {
            get {
                return amount;
            }

            set {
                amount = value;
            }
        }
    }
}
