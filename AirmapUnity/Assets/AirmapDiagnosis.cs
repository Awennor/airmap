﻿using Assets.Scripts.ConceptMap.model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirmapDiagnosis : MonoBehaviour {

    public AirMapModel model;
    
    ConceptMapDiagnosis diagnosis;
    public DiagnosisCommons diagnosisC;
    public bool generateCSV;

    internal ConceptMapDiagnosis DiagnosisData
    {
        get
        {
            return diagnosis;
        }

        set
        {
            diagnosis = value;
        }
    }

    public void Diagnosis()
    {
        DiagnosisPure();
        diagnosisC.DiagnosisToLogIntermediary();
        if (generateCSV)
        {
            string cvs = DiagnosisData.GenerateCSV();
            diagnosisC.Diagnosed(cvs);
        }
        else
        {
            diagnosisC.DiagnosedIntermediary_MapOnly(model.MapData);
        }

        //Debug.Log(cvs);
    }

    public void Diagnosis_ForceFinal()
    {
        DiagnosisPure();
        diagnosisC.DiagnoseAndLogFinal_mapOnly();
    }

    public void DiagnosisPure()
    {
        var targetMap = model.TargetMap;
        var builtMap = model.MapData;
        DiagnosisData.BuildDiagnosis(builtMap, targetMap);
    }

    // Use this for initialization
    void Start () {
        diagnosis = diagnosisC.DiagnosisData;
        
	}



    // Update is called once per frame
    void Update () {
		
	}
}
