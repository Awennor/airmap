﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectionCandidate : MonoBehaviour {

    public GameObject line;
    public GraphTest graphTest;
    float lineWidth = 10;


	// Use this for initialization
	void Start () {
        graphTest.OnRequestNodeAction.AddListener(LineShow);
        graphTest.OnNotTwoNodesSelected.AddListener(LineHide);
	}

    private void LineHide()
    {
        line.SetActive(false);
    }

    private void LineShow()
    {
        line.SetActive(true);
    }


    // Update is called once per frame
    void Update () {
        var first = graphTest.FirstNodeClicked;
        var second = graphTest.SecondNodeClicked;
        if (first != null && second != null) {
            var arg2 = first.transform.position;
            var arg3 = second.transform.position;
            var dis = (arg3 - arg2);
            var lineTransform = line.transform;
            lineTransform.rotation = Quaternion.Euler(0, 0, Mathf.Atan(dis.y / dis.x) * Mathf.Rad2Deg);
            lineTransform.position = arg2 + dis / 2;
            float scaleX = dis.magnitude;
            var lineRectT = lineTransform.GetComponent<RectTransform>();
            lineRectT.sizeDelta = new Vector2(scaleX, lineWidth);
        }
	}
}
