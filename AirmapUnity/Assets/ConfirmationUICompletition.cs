﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ConfirmationUICompletition : MonoBehaviour
{

    public GameObject UI;
    public Button completitionButton;
    public Button completitionConfirmationButton;
    public UnityEvent OnCompletition;
    public bool skipUI;

    // Use this for initialization
    void Start()
    {
        completitionConfirmationButton.onClick.AddListener(ConfirmButtonPress);
        completitionButton.onClick.AddListener(() =>
        {
            if (skipUI)
            {
                Confirm(); 
            }
            else
                UI.gameObject.SetActive(true);
        });
    }

    private void ConfirmButtonPress()
    {
        Confirm();
    }

    private void Confirm()
    {
        OnCompletition.Invoke();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
