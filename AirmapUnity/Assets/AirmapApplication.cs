﻿using pidroh.conceptmap.model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirmapApplication : MonoBehaviour {

    public TextAsset mapJson;
    public GraphTest graphTest;
    public ConnectionManager connectionManager;
    Dictionary<string, int> linkQuantities = new Dictionary<string, int>();
    ConceptMapData map;
    public AirMapModel model;
    public ConnectionChoserUIManager connectionChooser;

    public ConceptMapData Map
    {
        get
        {
            return map;
        }

        set
        {
            map = value;
        }
    }

    // Use this for initialization
    void Awake() {

        var config = GenericDataHolder.Instance.GetObject<AirmapConfiguration>();
        connectionChooser.HidableLinksState(config.HidableLinks);
        if (config.Jsonmap != null) {
            mapJson = config.Jsonmap;
            Debug.Log("Json map set");
        }

        Map = ConceptMapDataUtils.LoadFromJson(mapJson.text);
        model.SetTargetMap(map);  
        var nodeNames = Map.NodeNames;
        graphTest.nodeNames.Clear();
        graphTest.nodeNames.AddRange(nodeNames);

        var linkNames = Map.LinkNames;
        for (int i = 0; i < linkNames.Count; i++)
        {
            if (linkQuantities.ContainsKey(linkNames[i]))
            {
                linkQuantities[linkNames[i]]++;
            }
            else {
                linkQuantities.Add(linkNames[i], 1);
            }
        }

        var keys = linkQuantities.Keys;
        var connections = connectionManager.Connections;
        connections.Clear();
        foreach (var k in keys)
        {
            connections.Add(new ConnectionManager.ConnectionStatement(k, linkQuantities[k]));
        }

        

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
