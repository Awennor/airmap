﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackView : MonoBehaviour {

    public FeedbackUnitUI prefab;

    public GameObject[] containers;
    public List<FeedbackUnitUI>[] units;

    public event Action OnEndButtonPress;
    public Button endButton;
    public GameObject perfectMapObj;

	// Use this for initialization
	void Awake () {
        units = new List<FeedbackUnitUI>[containers.Length];
        for (int i = 0; i < units.Length; i++)
        {
            units[i] = new List<FeedbackUnitUI>();
        }
        endButton.onClick.AddListener(EndButtonPress);
	}

    private void EndButtonPress()
    {
        OnEndButtonPress();
    }

    internal void AmountOnContainer(int uiContainer, int count)
    {
        for (int i = 0; i < count; i++)
        {
            var u = Instantiate(prefab, containers[uiContainer].transform);
            units[uiContainer].Add(u);
        }
        containers[uiContainer].SetActive(count != 0);
        
    }

    // Update is called once per frame
    void Update () {
		
	}

    internal void SetTexts(int container, int i, string n1, string n2, string l)
    {
        units[container][i].SetTexts(n1,n2,l);
    }

    internal void ResultType(bool perfectMap)
    {
        perfectMapObj.SetActive(perfectMap);
    }

    internal void Hide(int uiContainer, int i)
    {
        units[uiContainer][i].gameObject.SetActive(false);
    }

    internal void CheckVisibilities()
    {
        for (int i = 0; i < containers.Length; i++)
        {
            containers[i].gameObject.SetActive(false);
            foreach (var item in units[i])
            {
                if (item.gameObject.activeSelf) {
                    containers[i].gameObject.SetActive(true);
                    break;
                }
            }
            
        }
    }
}
