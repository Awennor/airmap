﻿using Assets.Scripts.ConceptMap.model;
using pidroh.conceptmap.model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeedbackSceneConfig {

    public ConceptMapDiagnosis Diagnosis { set; get; }
    public ConceptMapData MapData { get; set; }
    public Action SceneDoneFunction { get; set; }

}
