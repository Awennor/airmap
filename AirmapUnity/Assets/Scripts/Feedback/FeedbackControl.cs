﻿using Assets.Scripts.ConceptMap.model;
using pidroh.conceptmap.model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FeedbackControl : MonoBehaviour
{

    public FeedbackView view;
    private bool perfectMap;

    // Use this for initialization
    void Start()
    {

        ConceptMapDiagnosis mapDiagnosis = null;
        ConceptMapData mapData = null;

        var config = GenericDataHolder.Instance.GetObject<FeedbackSceneConfig>();
        if (config == null)
        {
            mapData = new ConceptMapData();
            mapDiagnosis = new ConceptMapDiagnosis();
            mapData.NodeNames.Add("bla1");
            mapData.NodeNames.Add("bla2");
            mapData.LinkNames.Add("bl2");
            mapData.LinkNames.Add("bl3");
            mapDiagnosis.Missing.Add(new Proposition(0, 1, 0));
            mapDiagnosis.Missing.Add(new Proposition(1, 0, 1));
            mapDiagnosis.Incorrect.Add(new Proposition(1, 0, 1));
        }
        else
        {
            mapData = config.MapData;
            mapDiagnosis = config.Diagnosis;
        }



        FillAContainer(mapData, 0, mapDiagnosis.Missing);
        FillAContainer(mapData, 1, mapDiagnosis.Incorrect);

        perfectMap = mapDiagnosis.Missing.Count == 0 && mapDiagnosis.Incorrect.Count == 0;
        view.ResultType(perfectMap);
        view.OnEndButtonPress += View_OnEndButtonPress;
    }

    private void FillAContainer(ConceptMapData mapData, int uiContainer, List<Proposition> propList)
    {
        view.AmountOnContainer(uiContainer, propList.Count);
        for (int i = 0; i < propList.Count; i++)
        {
            Proposition prop = propList[i];
            if (prop.AllPositive())
            {
                var n1 = mapData.NodeNames[prop.Node1];
                var n2 = mapData.NodeNames[prop.Node2];
                var l = mapData.LinkNames[prop.Link];

                view.SetTexts(uiContainer, i, n1, n2, l);
            }
            else {
                view.Hide(uiContainer, i);
            }
            
        }
        view.CheckVisibilities();
    }

    private void View_OnEndButtonPress()
    {

        var config = GenericDataHolder.Instance.GetObject<FeedbackSceneConfig>();
        if (config != null)
            config.SceneDoneFunction();
        SceneManager.UnloadSceneAsync("Feedback");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
