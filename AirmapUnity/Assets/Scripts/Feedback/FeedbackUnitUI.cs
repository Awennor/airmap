﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackUnitUI : MonoBehaviour {
    public Text node1, node2, link;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    internal void SetTexts(string n1, string n2, string l)
    {
        node1.text = n1;
        node2.text = n2;
        link.text = l;
    }
}
