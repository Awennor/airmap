﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class MyIntEvent : UnityEvent<int> { }
[System.Serializable]
public class MyVector3Event : UnityEvent<Vector3> { }
[System.Serializable]
public class MyFloatEvent : UnityEvent<float> { }
[System.Serializable]
public class MyBoolEvent : UnityEvent<bool> { }
[System.Serializable]
public class MyGameObjectEvent : UnityEvent<GameObject> { }
[System.Serializable]
public class MyMonoBehaviourEvent : UnityEvent<MonoBehaviour> { }
[System.Serializable]
public class MyMonoBehaviourBoolEvent : UnityEvent<MonoBehaviour, bool> { }
[System.Serializable]
public class MyGameObjectListEvent : UnityEvent<List<GameObject>> { }
[System.Serializable]
public class MyTransformEvent : UnityEvent<Transform> { }
[System.Serializable]
public class MyStringEvent : UnityEvent<string> { }
[System.Serializable]
public class MyStringStringEvent : UnityEvent<string, string> { }
[System.Serializable]
public class MySpriteEvent : UnityEvent<Sprite> { }