﻿using FullSerializer;
using System;
using System.Collections;
using System.Collections.Generic;

namespace pidroh.conceptmap.model
{
    //[MessagePackObject(keyAsPropertyName: true)]
    public class ConceptMapData
    {
        public string Title { get; set; } = null;
        public List<string> NodeNames { get; set; } = new List<string>();
        public List<string> LinkNames
        { get; set; } = new List<string>();
        public List<Proposition> Propositions
        {
            get;

            set;
        } = new List<Proposition>();
    }

    //[MessagePackObject(keyAsPropertyName: true)]
    public struct Proposition {
        int node1, node2, link;

        public Proposition(int node1, int node2, int link)
        {
            this.node1 = node1;
            this.node2 = node2;
            this.link = link;
        }
        [fsProperty]
        public int Node1
        {
            get
            {
                return node1;
            }

            set
            {
                node1 = value;
            }
        }
        [fsProperty]
        public int Node2
        {
            get
            {
                return node2;
            }

            set
            {
                node2 = value;
            }
        }
        [fsProperty]
        public int Link
        {
            get
            {
                return link;
            }

            set
            {
                link = value;
            }
        }

        internal bool AllPositive()
        {
            return link >= 0 && node1 >= 0 && node2 >= 0;
        }
    }
}