﻿using EpForceDirectedGraph.cs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.Events;
using pidroh.conceptmap.model;
using static pidroh.conceptmap.airmap.view.ViewStructures;

public class GraphTest : MonoBehaviour, IGraphEventListener {

    GraphRenderer graphRend;
    Dictionary<string, NodeComp> nodeDrawers = new Dictionary<string, NodeComp>();
    Dictionary<string, GameObject> edgeDrawers = new Dictionary<string, GameObject>();

    public NodeComp nodeDrawMatrix;
    public GameObject edgeDrawMatrix;
    public RectTransform parentEdge;
    public RectTransform parentNode;
    public RectTransform mapParent;

    public float scalePosition = 1f;
    private NodeComp firstNodeClicked;
    private NodeComp secondNodeClicked;
    private Graph graph;
    public float edgeLength = 10f;

    public List<string> nodeNames;
    public float centerSuck = 1f;
    private ForceDirected2D physics;
    public bool InsideBounds;

    public CanvasScaler canvas;
    [SerializeField]
    private float drawingTimeScale = 1;

    

    public UnityEvent OnRequestNodeAction;
    public UnityEvent OnNotTwoNodesSelected;

    public float repulsion = 2500;
    public MyStringEvent On1stNodeChosen_Label;
    public MyStringEvent On2ndNodeChosen_Label;
    public MyStringEvent OnLinkDestroy;
    public UnityEvent OnClickOnLink;
    public UnityEvent OnClickNodeNormal;
    private NodeComp DragNodeBuffer;
    private NodeComp dragNodeBuffer2;
    private float dragBufferStartTime;
    public float dragBufferTimeNecessary;
    public int mapUpdatePerFrame;

    [SerializeField]
    bool updateTextLabels = true;
    [SerializeField]
    bool updateNodeType = true;

    public event Action<NodeSelectChange> OnNodeSelectedChange;
    public event Action<PropositionMade> OnConnected;
    public event Action<PropositionMade> OnDisconnected;
    public event Action<string> OnNodeClick, OnNodeClickEnd;

    public NodeComp FirstNodeClicked
    {
        get
        {
            return firstNodeClicked;
        }

        set
        {
            firstNodeClicked = value;
        }
    }

    public NodeComp SecondNodeClicked
    {
        get
        {
            return secondNodeClicked;
        }

        set
        {
            secondNodeClicked = value;
        }
    }

    public Dictionary<string, NodeComp> NodeDrawers
    {
        get
        {
            return nodeDrawers;
        }

        set
        {
            nodeDrawers = value;
        }
    }

    public bool UpdateTextLabels
    {
        get
        {
            return updateTextLabels;
        }

        set
        {
            updateTextLabels = value;
        }
    }

    public Dictionary<string, GameObject> EdgeDrawers
    {
        get
        {
            return edgeDrawers;
        }

        set
        {
            edgeDrawers = value;
        }
    }

    public bool UpdateNodeType
    {
        get
        {
            return updateNodeType;
        }

        set
        {
            updateNodeType = value;
        }
    }

    // Use this for initialization
    void Start() {
        graph = new Graph();

        List<NodeData> listOfDatas = new List<NodeData>();
        for (int i = 0; i < nodeNames.Count; i++)
        {
            string nodeName = nodeNames[i];
            NodeDataCustom customData = CreateNode(nodeName);

            listOfDatas.Add(customData);
        }
        //graph.CreateNodes(listOfDatas);
        graph.AddGraphListener(this);
        var nodes = graph.nodes;
        for (int i = 0; i < nodes.Count; i++) {
            //Debug.Log(nodes[i].Data.GetType());Debug.Log(nodes[i].Data is NodeDataCustom);
        }

        //m_fdgGraph.AddNode(new Node("0"));

        //graph.CreateEdge(n1, n2);
        //graph.CreateEdge(n3, n4);
        //graph.CreateEdge(n4, n5);
        //graph.CreateEdge(n3, n5);
        //graph.CreateEdge(n7, n6);

        physics = new ForceDirected2D(graph, 90, 3000, 0.7f);


        //physics.Threadshold = 0.2f;

        graphRend = new GraphRenderer(physics);

        graphRend.OnDrawEdge += GraphRend_OnDrawEdge;
        graphRend.OnDrawNode += GraphRend_OnDrawNode;
    }

    private NodeDataCustom CreateNode(string nodeName)
    {
        NodeDataCustom customData = new NodeDataCustom();
        customData.label = nodeName;
        customData.NodeType = NodeDataCustom.NodeType_Normal;
        customData.initialPostion = new FDGVector2(UnityEngine.Random.Range(0f, 5f), UnityEngine.Random.Range(0f, 5f));
        var node = graph.CreateNode(customData);
        var id = node.ID;
        AddNodeDrawer(id);
        return customData;
    }

    private void GraphRend_OnDrawNode(Node arg1, Vector3 arg2) {
        var id = arg1.ID;
        var node = graph.GetNodeById(id);
        

        NodeComp drawer = null;
        if (nodeDrawers.ContainsKey(id)) {
            drawer = nodeDrawers[id];
        } else {
            drawer = AddNodeDrawer(id);

        }
        if (updateNodeType && arg1.Data is NodeDataCustom) {

            NodeDataCustom nodeCustom = (NodeDataCustom)arg1.Data;
            drawer.NodeDataCustom = nodeCustom;
            drawer.GetComponent<GenericPropertySetterByEvent>().SetIntProperty("nodetype", nodeCustom.NodeType, true);
            //Debug.Log("CUSTOM NODE DATA");
        } else {
            //Debug.Log("NODE DATA NOT " + arg1.Data.GetType());
        }
        drawer.LatestPos_GraphSpace = arg2;
        drawer.transform.localPosition = arg2 * scalePosition;
        drawer.transform.localScale = Vector3.one;
        if (updateTextLabels) {
            Text textComp = drawer.GetComponentInChildren<Text>();
            var previousText = textComp.text;
            if (previousText != arg1.Data.label)
            {
                textComp.text = arg1.Data.label;
            }
        }
    }

    private NodeComp AddNodeDrawer(string id) {
        var draw = Instantiate(nodeDrawMatrix);
        nodeDrawers.Add(id, draw);
        draw.transform.SetParent(parentNode.transform);
        draw.name = id;
        draw.OnNodePointerDown.AddListener(() => { NodeClick(draw); });
        draw.OnNodePointerUp.AddListener(() => { NodeClickEnd(draw); });
        draw.OnNodePointerEnter.AddListener(() => { NodeCursorEnter(draw); });
        draw.OnNodePointerExit.AddListener(() => { NodeCursorExit(draw); });
        return draw;
    }

    private void NodeCursorEnter(NodeComp draw) {
        var arg1 = graph.GetNodeById(draw.name);
        float timeDiff = Time.time - dragBufferStartTime;
        if (arg1.Data is NodeDataCustom) {
            NodeDataCustom nodeDataCustom = (arg1.Data as NodeDataCustom);
            if (nodeDataCustom.NodeType != NodeDataCustom.NodeType_Link){
                if (DragNodeBuffer != null && draw != DragNodeBuffer && timeDiff > this.dragBufferTimeNecessary) {
                    dragNodeBuffer2 = draw;
                    
                    SelectNodeSet(dragNodeBuffer2, true);
                    //dragNodeBuffer2.GetComponent<Animator>().SetTrigger("Pressed");
                }
            }
        }


    }

    private void SelectNodeSet(NodeComp node, bool selected)
    {
        bool old = node.NodeSelected;
        node.NodeSelected = selected;
        if (old != selected) {
            if (OnNodeSelectedChange != null) {
                string text = node.Label.text;
                OnNodeSelectedChange(new NodeSelectChange(selected,text));
            }
        }

    }

    private void NodeCursorExit(NodeComp draw) {
        if (draw == dragNodeBuffer2) {
            SelectNodeSet(dragNodeBuffer2, false);
            
            //dragNodeBuffer2.GetComponent<Animator>().SetTrigger("Normal");
            dragNodeBuffer2 = null;
        }
    }

    private void NodeClickEnd(NodeComp draw) {
        if (OnNodeClickEnd != null) {
            OnNodeClickEnd(draw.name);
        }
        if (DragNodeBuffer != null && dragNodeBuffer2 != null) {
            if (DragNodeBuffer != FirstNodeClicked) {
                UnselectFirstNode();
                FirstNodeClicked = null;
                FirstNodeClicked = dragNodeBuffer2;
            } else {
                SecondNodeClicked = dragNodeBuffer2;
            }

            OnRequestNodeAction.Invoke();
            //NodeClick(dragNodeBuffer2);
            DragNodeBuffer = null;
            dragNodeBuffer2 = null;
        }
    }

    private void NodeClick(NodeComp draw)
    {
        if (OnNodeClick != null) {
            OnNodeClick(draw.name);
        }
        string id = draw.name;
        var arg1 = graph.GetNodeById(id);
        this.DragNodeBuffer = draw;
        this.dragBufferStartTime = Time.time;

        if (arg1.Data is NodeDataCustom)
        {
            NodeDataCustom nodeCustom = (NodeDataCustom)arg1.Data;
            if (nodeCustom.NodeType == NodeDataCustom.NodeType_Normal)
            { //if node is normal
                if (draw == SecondNodeClicked)
                {
                    //if clicked node is the second node clicked
                    //deselect second node
                    UnselectSecondNode();
                    SecondNodeClicked = null;

                }
                else
                {
                    if (FirstNodeClicked != null)
                    { //first node ready

                        if (draw == FirstNodeClicked)
                        { //select same node, so deselect
                            if (OnClickNodeNormal != null)
                                OnClickNodeNormal.Invoke();

                            UnselectFirstNode();
                            FirstNodeClicked = null;
                            DragNodeBuffer = null;

                            //UnselectAll();
                        }
                        else
                        {
                            //select second node
                            UnselectSecondNode();
                            SecondNodeClicked = draw;
                            On2ndNodeChosen_Label.Invoke(graph.GetNodeById(draw.name).Data.label);

                            
                            SelectNodeSet(secondNodeClicked, true);


                            /*
                            var goName = draw.name;
                            var data = new EdgeData();
                            data.length = edgeLength;
                            graph.CreateEdge(goName, previousNodeClicked.name, data);
                            previousNodeClicked = null;
                            */
                        }

                    }
                    else
                    {
                        //select first node
                        //UnselectAll();
                        //UnselectSecondNode();
                        UnselectFirstNode();
                        FirstNodeClicked = draw;
                        var nodeId = draw.name;
                        On1stNodeChosen_Label.Invoke(graph.GetNodeById(nodeId).Data.label);

                        NodeComp nodeComp = FirstNodeClicked.GetComponent<NodeComp>();
                        SelectNodeSet(nodeComp, true);
                    }
                }


            }
            else
            { //link node cliking
                OnClickOnLink.Invoke();
                LinkDestroy(draw, id);
            }
        }
        else
        { //not a custom node
        }

        CheckForTwoNodeSelectedAction();

    }

    private void LinkDestroy(NodeComp draw, string id)
    {
        var nodeLink = graph.GetNodeById(id);
        string label = nodeLink.Data.label;

        Node node1, node2;
        GetTwoGraphNodeIdsConnectedToGraphNode(id, out node1, out node2);
        OnLinkDestroy.Invoke(label);
        graph.RemoveNode(nodeLink);
        graph.RemoveEdgeByNodeID(id);
        if (OnDisconnected != null)
            OnDisconnected(new PropositionMade(node1.Data.label, node2.Data.label, nodeLink.Data.label, node1.ID, node2.ID, id));
        draw.gameObject.SetActive(false);
    }

    private void GetTwoGraphNodeIdsConnectedToGraphNode(string id, out Node node1, out Node node2)
    {
        var edges = graph.edges;
        int nodesAssigned = 0;
        node1 = null;
        node2 = null;
        for (int i = 0; i < edges.Count; i++)
        {
            if (edges[i].Source.ID == id) {
                if (nodesAssigned == 0)
                    node1 = edges[i].Target;
                if (nodesAssigned == 1)
                    node2 = edges[i].Target;
                nodesAssigned++;
            }
            if (edges[i].Target.ID == id)
            {
                if (nodesAssigned == 0)
                    node1 = edges[i].Source;
                if (nodesAssigned == 1)
                    node2 = edges[i].Source;
                nodesAssigned++;
            }
            if (nodesAssigned == 2) {
                return;
            }
        }
    }

    private void CheckForTwoNodeSelectedAction()
    {
        if (SecondNodeClicked != null && FirstNodeClicked != null)
        {
            OnRequestNodeAction.Invoke();
        }
        else
        {
            OnNotTwoNodesSelected.Invoke();
        }
    }

    private void UnselectFirstNode() {
        if (FirstNodeClicked != null) {
            SelectNodeSet(firstNodeClicked, false);
            
        }
    }

    private void UnselectSecondNode() {
        if (SecondNodeClicked != null) {
            
            SelectNodeSet(secondNodeClicked, false);
        }
    }

    private void UnselectAll() {
        foreach (var v in nodeDrawers) {
            SelectNodeSet(v.Value, false);
        }
    }

    public void CreateConnectionBetweenNodesChosen_External(string connectionLabel)
    {
        var node1 = SecondNodeClicked;
        var node2 = FirstNodeClicked;
        ConnectNodes(connectionLabel, node1, node2);
    }

    private void ConnectNodes(string connectionLabel, NodeComp node1, NodeComp node2)
    {
        var pos1 = node1.LatestPos_GraphSpace;
        var pos2 = node2.LatestPos_GraphSpace;
        string nodeLinkId = CreateLinkNode(connectionLabel, (pos1 + pos2) / 2);


        CreateEdge(node1.name, nodeLinkId);
        CreateEdge(nodeLinkId, node2.name);

        if (OnConnected != null)
        {
            OnConnected(new PropositionMade(node1.Label.text, node2.Label.text, connectionLabel, node1.name, node2.name, nodeLinkId));
        }

        //CreateEdge(node1.name, node2.name);
        DeselectNodes();
    }

    internal void CreateLink(string node, string node1, string link)
    {
        NodeComp n1 = GetNodeByLabel(node);
        ConnectNodes(link, n1, GetNodeByLabel(node1));
    }

    private NodeComp GetNodeByLabel(string node)
    {
        var ni = nodeNames.IndexOf(node);
        return NodeDrawers[ni + ""];
    }

    public void DeselectNodes()
    {
        UnselectFirstNode();
        UnselectSecondNode();
        FirstNodeClicked = null;
        SecondNodeClicked = null;
        CheckForTwoNodeSelectedAction();
    }

    private string CreateLinkNode(string connectionLabel, Vector3 pos) {

        NodeDataCustom nodeDataCustom = new NodeDataCustom(connectionLabel, NodeDataCustom.NodeType_Link);
        
        nodeDataCustom.initialPostion = new FDGVector2(pos.x, pos.y);
        var node = graph.CreateNode(nodeDataCustom);
        AddNodeDrawer(node.ID);
        
        return node.ID;
    }

    private void CreateEdge(string id, string id2) {
        //make every instance of edge draw have same name as id
        var data = new EdgeData();
        data.length = edgeLength;
        var edge = graph.CreateEdge(id, id2, data);
        AddEdgeDrawer(edge.ID);
        //Debug.Log(edge.ID+" EDGE CRAZY PEDROOOO CALLED? +id");

    }

    private void GraphRend_OnDrawEdge(Edge arg1, Vector3 arg2, Vector3 arg3) {
        //Debug.Log("EDGE DRAW");



        //RefreshEdgeDraw();
        var id = arg1.ID;
        GameObject drawer;
        if (edgeDrawers.ContainsKey(id)) {
            drawer = edgeDrawers[id];
            //Debug.Log(drawer+drawer.name);

        } else {
            drawer = AddEdgeDrawer(id);
        }
        drawer.name = id;
        drawer.transform.localScale = new Vector3(1, 1, 1);
        drawer.transform.localPosition = (arg2 + arg3) * scalePosition / 2;
        var dis = (arg3 - arg2) * scalePosition * canvas.scaleFactor;
        var lineTransform = drawer.transform.Find("line").transform;
        lineTransform.rotation = Quaternion.Euler(0, 0, Mathf.Atan(dis.y / dis.x) * Mathf.Rad2Deg);
        float scaleX = dis.magnitude / canvas.scaleFactor;
        var lineRectT = lineTransform.GetComponent<RectTransform>();
        lineRectT.sizeDelta = new Vector2(scaleX, 5);
        //lineRectT. hasChanged width and not scaleX



        //lineTransform.localScale = new Vector3(scaleX, 1, 1);

    }

    private GameObject AddEdgeDrawer(string id) {
        var edgeDraw = Instantiate(edgeDrawMatrix);
        edgeDraw.name = id;
        //Debug.Log("NAME IS ID");
        edgeDraw.transform.localScale = new Vector3(1, 1, 1);
        edgeDrawers.Add(id, edgeDraw);
        edgeDraw.transform.SetParent(parentEdge.transform);
        //Debug.Log("add edge drawer " + id);
        return edgeDraw;
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetMouseButtonUp(0)) {
            DragNodeBuffer = null;
            Debug.Log("Clear Drag buffer");
        }
        physics.CenterSuck = centerSuck;
        physics.Repulsion = repulsion;
        for (int i = 0; i < mapUpdatePerFrame; i++)
        {
            graphRend.Draw(Time.deltaTime * drawingTimeScale);
        }
        
        var box = physics.GetBoundingBox();
        var bLeft = GraphRenderer.Vector3Get(box.bottomLeftFront) * scalePosition;
        var tRight = GraphRenderer.Vector3Get(box.topRightBack) * scalePosition;
        var containingRect = parentNode.rect;
        var size = tRight - bLeft;
        var currentSize = new Vector3(containingRect.size.x, containingRect.size.y);
        var xScale = size.x / currentSize.x;
        var yScale = size.y / currentSize.y;
        var bestScale = xScale;

        if (bestScale < yScale) bestScale = yScale;
        bestScale = 1 / bestScale;
        float maxScale = 1.2f;
        if (bestScale > maxScale)
        {
            bestScale = maxScale;
        }
        mapParent.localScale = new Vector3(bestScale, bestScale, bestScale);
        if (bestScale < 1) bestScale -= 0.05f;
        if (bestScale > 1.3f || bestScale < 1f) {

            //canvas.scaleFactor *= bestScale;
        }
        
        //bestScale += 0.1f;

        InsideBounds = containingRect.Contains(bLeft) && containingRect.Contains(tRight);

        //var disToMin = new Vector3(containingRect.min.x, containingRect.min.y) - bLeft;
        //var disToMax = new Vector3(containingRect.max.x, containingRect.max.y) - tRight;
        //disToMin *= -1;

    }


    private void RefreshEdgeDraw() {
        foreach (var eD in edgeDrawers) {
            var edge = graph.GetEdgeByID(eD.Key);
            var gameObject1 = eD.Value.gameObject;
            var edgeExist = edge != null;
            gameObject1.SetActive(edgeExist);

            if (edgeExist == false) {
                //Debug.Log(eD.Key + "EDGE DELETED" + eD.Value.gameObject.name);
            } else {
                //Debug.Log(edge.ID+"EDGE ID GRAPH");
                //Debug.Log(eD.Key + "EDGE NOT DELETED" + eD.Value.gameObject.name);
            }


        }
    }

    [ContextMenu("Clear Map")]
    public void ClearMap() {
        graph.RemoveAllNodes();
        graph.RemoveAllEdges();
        foreach (var item in nodeDrawers)
        {
            item.Value.gameObject.SetActive(false);
        }
    }

    [ContextMenu("Destroy All Links")]
    public void DestroyAllLinks() {
        foreach (var item in nodeDrawers)
        {
            
            if (item.Value.gameObject.active && item.Value.NodeDataCustom.NodeType == NodeDataCustom.NodeType_Link) {
                LinkDestroy(item.Value, item.Value.gameObject.name);
            }
        }
    }

    void IGraphEventListener.GraphChanged() {
        RefreshEdgeDraw();
    }
}
