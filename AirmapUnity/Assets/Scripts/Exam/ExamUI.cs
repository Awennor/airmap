﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Exam
{
    public class ExamUI : MonoBehaviour
    {
        public MultipleAnswerUI multipleAnswerUI;
        public event Action<int, int> OnQuestionAnswered;
        public Button diagnosisButton;
        public Text title;

        public void Start()
        {
            multipleAnswerUI.gameObject.SetActive(false);
            
        }

        private void MultipleAnswerUI_OnQuestionAnswered(int arg1, int id)
        {
            if (OnQuestionAnswered != null)
                OnQuestionAnswered(arg1, id);
        }

        public void AddMultipleAnswerQuestion(int id, string questionText, List<string> items) {
            var mu = Instantiate(multipleAnswerUI);
            mu.gameObject.SetActive(true);
            mu.transform.SetParent(multipleAnswerUI.transform.parent);
            mu.SetItems(items);
            mu.questionText.text = questionText;
            mu.OnQuestionAnswered += (item, mul)=> {
                MultipleAnswerUI_OnQuestionAnswered(item, id);
            };
        }

        [ContextMenu("test")]
        public void Test() {
            //AddMultipleAnswerQuestion(0, "What", new string[] {"bla", "blu"});
        }

        internal void TitleText(string examLabel)
        {
            title.text = examLabel;
        }
    }
}
