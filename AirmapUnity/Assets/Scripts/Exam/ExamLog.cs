﻿using Assets.Scripts.Exam;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Pidroh.UnityUtils.LogDataSystem;

public class ExamLog : MonoBehaviour {

    public ExamModel examModel;
    public ExamController examControl;
    public LogCapturerManager log;
    private string examLabel;
    public event Action OnLoggingDone;
    public DiagnosisCommons diagnosisCommons;

    // Use this for initialization
    void Start () {
        examLabel = examModel.Exam.ExamLabel;
        log.DirectLog("exam_start", examLabel);

        examControl.OnQuestionAnswered += ExamControl_OnQuestionAnswered;
        examControl.OnDiagnosisRequest += ExamControl_OnDiagnosisRequest;
        examControl.OnDiagnosisEnd += ExamControl_OnDiagnosisEnd;
        examModel.DiagnoseCallback += ExamModel_DiagnoseCallback;
        examModel.Diagnose_QuestionNotAnswered += ExamModel_Diagnose_QuestionNotAnswered;

        
	}

    private void ExamControl_OnDiagnosisEnd()
    {

        log.DirectLog("diagnosis_end", examLabel);
        if (OnLoggingDone != null)
            OnLoggingDone();
        diagnosisCommons.DiagnosedFinal();
    }

    private void ExamControl_OnDiagnosisRequest()
    {
        log.DirectLog("diagnosis_start", examLabel);
    }

    private void ExamModel_Diagnose_QuestionNotAnswered(int obj)
    {
        log.DirectLog("question_notanswered", examLabel, obj+"");
    }

    private void ExamModel_DiagnoseCallback(int arg1, int arg2, bool arg3)
    {
        log.DirectLog("question_diagnosis", examLabel, arg1 + "", arg2+"", arg3+"");
    }

    private void ExamControl_OnQuestionAnswered(int arg1, int arg2, bool arg3)
    {
        log.DirectLog("question_answered", examLabel, arg1 + "", arg2 + "", arg3 + "");
    }

    // Update is called once per frame
    void Update () {
		
	}
}
