﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Exam
{

    public class ExamModel : MonoBehaviour {
        [SerializeField]
        private ExamData exam;
        List<Question> questionList = new List<Question>();
        private Dictionary<int, int> insertedAnswers = new Dictionary<int, int>();
        public event Action<int, int, bool> DiagnoseCallback;
        public event Action<int> Diagnose_QuestionNotAnswered;

        public ExamData Exam
        {
            get
            {
                return exam;
            }

            set
            {
                exam = value;
            }
        }

        public List<Question> FillQuestionList() {
            var questions = exam.Questions;
            var qIds = exam.QuestionIds;
            for (int i = 0; i < qIds.Count; i++)
            {
                var q = questions.GetQuestion(qIds[i]);
                questionList.Add(q);
            }
            return questionList;
        }

        internal bool IsQuestionCorrect(int item, int questionId)
        {
            var q = exam.Questions.GetQuestion(questionId);
            if (q != null) {
                var maq = q as MultipleAnswerQuestion;
                if (maq != null)
                {
                    return maq.Answer == item;
                }
            }
            Debug.LogError("Multiple Answer Question not found");
            return false;

        }

        internal bool DoAllQuestionsHaveAnswers()
        {
            var qIds = exam.QuestionIds;
            foreach (var q in qIds)
            {
                if (!insertedAnswers.ContainsKey(q)) {
                    return false;
                }
            }
            return true;
        }

        internal void RegisterAnswer(int item, int questionId)
        {
            if (insertedAnswers.ContainsKey(questionId))
            {
                insertedAnswers[questionId] = item;
            }
            else {
                insertedAnswers.Add(questionId, item);
            }
            
        }

        public void Diagnose() {
            var qIds = exam.QuestionIds;
            foreach (var q in qIds)
            {
                int answer;
                if (insertedAnswers.TryGetValue(q, out answer))
                {
                    bool correct = IsQuestionCorrect(answer, q);
                    if (DiagnoseCallback != null)
                        DiagnoseCallback(q, answer, correct);
                }
                else {
                    if (Diagnose_QuestionNotAnswered != null)
                        Diagnose_QuestionNotAnswered(q);
                }
            }
        }
    }

    [Serializable]
    public class ExamData
    {
        [SerializeField]
        string examLabel;
        [SerializeField]
        List<int> questionIds = new List<int>();
        [SerializeField]
        QuestionDatabase questions = new QuestionDatabase();
        

        public List<int> QuestionIds
        {
            get
            {
                return questionIds;
            }

            set
            {
                questionIds = value;
            }
        }

        public QuestionDatabase Questions
        {
            get
            {
                return questions;
            }

            set
            {
                questions = value;
            }
        }

        public string ExamLabel
        {
            get
            {
                return examLabel;
            }

            set
            {
                examLabel = value;
            }
        }
    }

    [Serializable]
    public class QuestionDatabase
    {
        [SerializeField]
        List<MultipleAnswerQuestion> multipleAnswer = new List<MultipleAnswerQuestion>();
        [SerializeField]
        List<LikertScale> likert;

        internal void AddQuestion(MultipleAnswerQuestion maq)
        {
            multipleAnswer.Add(maq);
        }

        //List<Question> aux = new List<Question>();;

        internal Question GetQuestion(int id)
        {
            Question q = null;
            q = FindQuestionByID(multipleAnswer, id);
            if (q != null) return q;
            q = FindQuestionByID(likert, id);
            if (q != null) return q;
            return q;
        }

        private Question FindQuestionByID<T>(List<T> list, int id) where T:Question
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Id == id) {
                    return list[i];
                }
            }
            return null;
        }
    }

    [Serializable]
    public class Question {
        [SerializeField]
        int id;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
    }

    [Serializable]
    public class MultipleAnswerQuestion : Question
    {
        [SerializeField]
        string question;
        [SerializeField]
        List<string> items = new List<string>();
        [SerializeField]
        int answer;

        public string Question
        {
            get
            {
                return question;
            }

            set
            {
                question = value;
            }
        }

        public List<string> Items
        {
            get
            {
                return items;
            }

            set
            {
                items = value;
            }
        }

        public int Answer
        {
            get
            {
                return answer;
            }

            set
            {
                answer = value;
            }
        }
    }


    [Serializable]
    public class LikertScale  : Question
    {
        [SerializeField]
        string question;
        [SerializeField]
        int numberOfItems;
        [SerializeField]
        string[] labels;
    }


}
