﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Exam
{
    public class ExamController : MonoBehaviour
    {
        public ExamUI examUI;
        public ExamModel examModel;
        public event Action<int, int, bool> OnQuestionAnswered;
        public event Action OnDiagnosisRequest;
        public event Action OnDiagnosisEnd;

        public void Diagnose() {
            if(OnDiagnosisRequest != null)
                OnDiagnosisRequest();
            examModel.Diagnose();
            if (OnDiagnosisEnd != null)
                OnDiagnosisEnd();
        }
        
        public void Awake() {
            examUI.diagnosisButton.gameObject.SetActive(false);
            var examData = GenericDataHolder.Instance.GetObject<ExamData>();
            if (examData != null) {
                examModel.Exam = examData;
                
            }
            examUI.OnQuestionAnswered += ExamUI_OnQuestionAnswered;
            var questionList = examModel.FillQuestionList();
            for (int i = 0; i < questionList.Count; i++)
            {
                var q = questionList[i];
                var mq = q as MultipleAnswerQuestion;
                if (mq != null) {
                    examUI.AddMultipleAnswerQuestion(mq.Id,
                        (i+1)+") "+mq.Question, 
                        mq.Items);

                }
            }
            if(examData != null)
                examUI.TitleText(examData.ExamLabel);

        }

        private void ExamUI_OnQuestionAnswered(int item, int questionId)
        {
            examModel.RegisterAnswer(item, questionId);
            bool allHaveAnswers = examModel.DoAllQuestionsHaveAnswers();
            examUI.diagnosisButton.gameObject.SetActive(allHaveAnswers);
            bool correct = examModel.IsQuestionCorrect(item, questionId);
            if (OnQuestionAnswered != null) {
                OnQuestionAnswered(item, questionId, correct);
            }
        }

        


    }
}
