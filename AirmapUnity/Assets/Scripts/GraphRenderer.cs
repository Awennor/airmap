﻿using EpForceDirectedGraph.cs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class GraphRenderer : AbstractRenderer {

    public event Action<Edge, Vector3, Vector3> OnDrawEdge;
    public event Action<Node, Vector3> OnDrawNode;

    public GraphRenderer(IForceDirected iForceDirected) : base(iForceDirected) {
        
    }

    public override void Clear() {
    }

    protected override void drawEdge(Edge iEdge, AbstractVector iPosition1, AbstractVector iPosition2) {
        OnDrawEdge.Invoke(iEdge, Vector3Get(iPosition1), Vector3Get(iPosition2));
    }

    static public Vector3 Vector3Get(AbstractVector av) {
        return new Vector3(av.x, av.y, av.z);
    }

    protected override void drawNode(Node iNode, AbstractVector iPosition) {
        OnDrawNode.Invoke(iNode, Vector3Get(iPosition));
    }


}
