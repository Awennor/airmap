﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pidroh.conceptmap.airmap.view
{
    public class ViewStructures
    {
        
        public struct PropositionMade
        {
            string node1, node2, link;
            string node1UIId, node2UIId, linkUIId;


            public PropositionMade(string node1, string node2, string link, string node1UIId, string node2UIId, string linkUIId)
            {
                this.node1 = node1;
                this.node2 = node2;
                this.link = link;
                this.node1UIId = node1UIId;
                this.node2UIId = node2UIId;
                this.linkUIId = linkUIId;
            }

            public string Node1Name
            {
                get
                {
                    return node1;
                }

                set
                {
                    node1 = value;
                }
            }

            public string Node2Name
            {
                get
                {
                    return node2;
                }

                set
                {
                    node2 = value;
                }
            }

            public string LinkName
            {
                get
                {
                    return link;
                }

                set
                {
                    link = value;
                }
            }

            public string Node1UIId
            {
                get
                {
                    return node1UIId;
                }

                set
                {
                    node1UIId = value;
                }
            }

            public string Node2UIId
            {
                get
                {
                    return node2UIId;
                }

                set
                {
                    node2UIId = value;
                }
            }

            public string LinkUIId
            {
                get
                {
                    return linkUIId;
                }

                set
                {
                    linkUIId = value;
                }
            }
        }

        public struct NodeSelectChange
        {
            bool selected;
            string nodeName;

            public NodeSelectChange(bool selected, string nodeName)
            {
                this.selected = selected;
                this.nodeName = nodeName;
            }

            public bool Selected
            {
                get
                {
                    return selected;
                }

                set
                {
                    selected = value;
                }
            }

            public string NodeName
            {
                get
                {
                    return nodeName;
                }

                set
                {
                    nodeName = value;
                }
            }
        }
    }

}