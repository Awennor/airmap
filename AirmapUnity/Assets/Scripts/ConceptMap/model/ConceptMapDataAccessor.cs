﻿using pidroh.conceptmap.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.ConceptMap.model
{
    class ConceptMapDataAccessor
    {
        ConceptMapData data;
        public event Action<string> OnNodeAdded;
        public event Action<string> OnLinkAdded;
        public event Action<ConceptMapDataAccessor> OnChange;


        public ConceptMapDataAccessor(ConceptMapData data)
        {
            this.Data = data;
        }

        public void AddNodes(List<string> nodes)
        {
            data.NodeNames.AddRange(nodes);
            foreach (var n in nodes)
            {
                if (OnNodeAdded != null)
                    OnNodeAdded(n);

            }
            NotifyChange();
        }

        internal int GetNodeId(string node1)
        {
            return data.NodeNames.IndexOf(node1);
        }

        private void NotifyChange()
        {
            if (OnChange != null)
            {
                OnChange(this);
            }
        }

        internal int GetLinkId(string link)
        {
            return data.LinkNames.IndexOf(link);
        }

        public void AddLinks(List<string> links)
        {
            data.LinkNames.AddRange(links);
            foreach (var n in links)
            {
                if (OnLinkAdded != null)
                    OnLinkAdded(n);
            }
            NotifyChange();
        }

        internal void AddPropositionInfo(Proposition propositionData)
        {
            Debug.Log(string.Format("Concept map added proposition {0} {1} {2}", propositionData.Node1, propositionData.Node2, propositionData.Link));
            data.Propositions.Add(propositionData);
        }

        internal bool RemoveOnePropositionByNames(string linkName, string node1Name, string node2Name)
        {
            var ps = data.Propositions;
            Proposition equivalentP;
            bool hasEquivalent = HasEquivalent(ps, new Proposition(GetNodeId(node1Name), GetNodeId(node2Name), GetLinkId(linkName)), this.data, out equivalentP);
            if(hasEquivalent)
                data.Propositions.Remove(equivalentP);
            return hasEquivalent;

        }

        public ConceptMapData Data
        {
            get
            {
                return data;
            }

            set
            {
                data = value;
            }
        }

        internal void SetPropositionInfo(int nodeId, int linkId, int whichNode)
        {
            Proposition p;
            var foundProp = GetPropositionByLinkId(linkId, out p);
            
            if (foundProp)
            {
                int index = data.Propositions.IndexOf(p);
                if (whichNode == 0)
                {
                    p.Node1 = nodeId;
                }
                else
                {
                    p.Node2 = nodeId;
                }
                data.Propositions[index] = p;
            }

            NotifyChange();
            //data.Propositions[linkId].Node1
        }

        private bool GetPropositionByLinkId(int linkId, out Proposition pout)
        {
            if (data.Propositions[linkId].Link == linkId)
            {
                pout = data.Propositions[linkId];
                return true;
            }
            else
            {
                foreach (var p in data.Propositions)
                {
                    if (p.Link == linkId)
                    {
                        pout = p;
                        return true;
                    }

                }
            }
            pout = new Proposition();
            return false;

        }

        public void CreateOnePropositionForEachLink()
        {
            int linkAmount = data.LinkNames.Count;
            for (int i = 0; i < linkAmount; i++)
            {
                data.Propositions.Add(new Proposition(-1, -1, i));
            }
            NotifyChange();
        }

        public static bool HasEquivalent(List<Proposition> container, Proposition proposition, ConceptMapData map, out Proposition equivalentP)
        {
            equivalentP = default(Proposition);
            var p = proposition;
            for (int i = 0; i < container.Count; i++)
            {
                var p2 = container[i];


                if (Verify(p.Link, p2.Link, map.LinkNames))
                {
                    if (
                        (Verify(p.Node1, p2.Node1, map.NodeNames) && Verify(p.Node2, p2.Node2, map.NodeNames))
                        ||
                        (Verify(p.Node2, p2.Node1, map.NodeNames) && Verify(p.Node1, p2.Node2, map.NodeNames))
                        )
                    {
                        equivalentP = p2;
                        return true;
                    }


                }
            }
            return false;
        }

        private static bool Verify(int id1, int id2, List<string> names)
        {
            if (id1 == id2) return true;
            if (id1 >= 0 && id2 >= 0)
            {
                if (id1 >= names.Count)
                {
                    Debug.LogError("ID is above the count " + id1);
                }
                if (id2 >= names.Count)
                {
                    Debug.LogError("ID is above the count " + id2);
                }
                string name1 = names[id1];
                string name2 = names[id2];
                
                if (name1 == name2) return true;
            }

            return false;
        }
    }
}
