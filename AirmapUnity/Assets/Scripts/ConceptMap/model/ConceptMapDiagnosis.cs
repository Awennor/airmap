﻿using pidroh.conceptmap.model;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.ConceptMap.model
{


    public class ConceptMapDiagnosis
    {
        List<Proposition> correct = new List<Proposition>();
        List<Proposition> incorrect = new List<Proposition>();
        List<Proposition> missing = new List<Proposition>();
        StringBuilder stringBuilder = new StringBuilder();

        public List<Proposition> Correct
        {
            get
            {
                return correct;
            }

            set
            {
                correct = value;
            }
        }

        public List<Proposition> Incorrect
        {
            get
            {
                return incorrect;
            }

            set
            {
                incorrect = value;
            }
        }

        public List<Proposition> Missing
        {
            get
            {
                return missing;
            }

            set
            {
                missing = value;
            }
        }

        public void BuildDiagnosis(ConceptMapData built, ConceptMapData target)
        {
            correct.Clear();
            incorrect.Clear();
            missing.Clear();
            var targetPropositions = target.Propositions;
            missing.AddRange(targetPropositions);
            var builtPropositions = built.Propositions;
            for (int i = 0; i < builtPropositions.Count; i++)
            {
                var p = builtPropositions[i];
                Proposition equivalentP = default(Proposition);
                if (ConceptMapDataAccessor.HasEquivalent(container: missing, proposition: p, map: target, equivalentP: out equivalentP))
                {
                    correct.Add(p);
                    var count = missing.Count;

                    missing.Remove(equivalentP);
                    if (count <= missing.Count) {
                        Debug.Log("ERROR!");
                    }
                }
                else
                {
                    incorrect.Add(p);
                }
            }
        }

        public string GenerateCSV()
        {
            stringBuilder.Clear();
            var array = correct;
            var message = "correct";
            PropositionToCSV(array, message);
            PropositionToCSV(incorrect, "incorrect");
            PropositionToCSV(missing, "missing");
            return stringBuilder.ToString();
        }



        private void PropositionToCSV(List<Proposition> array, string message)
        {
            for (int i = 0; i < array.Count; i++)
            {
                var p = array[i];
                stringBuilder.Append(message);
                stringBuilder.Append(',');
                stringBuilder.Append(p.Node1);
                stringBuilder.Append(',');
                stringBuilder.Append(p.Node2);
                stringBuilder.Append(',');
                stringBuilder.Append(p.Link);


                stringBuilder.Append('\n');
            }
        }

        

        
    }
}
