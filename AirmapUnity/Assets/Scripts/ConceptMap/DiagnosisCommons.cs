﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.ConceptMap.model;
using UnityEngine;
using Pidroh.UnityUtils.LogDataSystem;
using pidroh.conceptmap.model;
using UnityEngine.SceneManagement;

public class DiagnosisCommons : MonoBehaviour
{

    internal ConceptMapDiagnosis DiagnosisData { get; set; } = new ConceptMapDiagnosis();
    public event Action<string> OnDiagnosedCSV;
    public event Action OnDiagnosedIntermediary;
    public event Action OnDiagnosedFinal;
    public LogCapturerManager logCapturerManager;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    internal void Diagnosed(string diagnosis)
    {
        if (OnDiagnosedCSV != null)
            OnDiagnosedCSV(diagnosis);
    }

    internal void DiagnosedIntermediary_MapOnly(ConceptMapData mapData)
    {
        
        if (OnDiagnosedIntermediary != null)
            OnDiagnosedIntermediary();
        var config = GenericDataHolder.Instance.GetOrCreateObject<FeedbackSceneConfig>();
        config.Diagnosis = DiagnosisData;
        config.MapData = mapData;
        config.SceneDoneFunction = FeedbackSceneOver;
        SceneManager.LoadSceneAsync("Feedback", LoadSceneMode.Additive);
    }

    private void FeedbackSceneOver()
    {
        var config = GenericDataHolder.Instance.GetOrCreateObject<FeedbackSceneConfig>();
        if (config.Diagnosis.Incorrect.Count == 0 
            && config.Diagnosis.Missing.Count == 0)
        {
            DiagnoseAndLogFinal_mapOnly();
        }
    }

    public void DiagnoseAndLogFinal_mapOnly() {
        DiagnosisToLog("diagnosis_final");
        DiagnosedFinal();
    }

    public void DiagnosedFinal()
    {
        OnDiagnosedFinal();
    }

    internal void DiagnosisToLogIntermediary()
    {
        DiagnosisToLog("diagnosis_intermediary");
    }

    private void DiagnosisToLog( string labelMain)
    {
        logCapturerManager.DirectLog(labelMain + "_start");
        var diagnosis = DiagnosisData;
        string label = "correct";
        var props = diagnosis.Correct;
        PropositionsToLog(labelMain, label, props);
        PropositionsToLog(labelMain, "incorrect", diagnosis.Incorrect);
        PropositionsToLog(labelMain, "missing", diagnosis.Missing);

    }

    private void PropositionsToLog(string labelMain, string label, List<pidroh.conceptmap.model.Proposition> props)
    {
        for (int i = 0; i < props.Count; i++)
        {
            var p = props[i];
            logCapturerManager.DirectLog(labelMain, label, p.Node1 + "", p.Node2 + "", p.Link + "");
        }
    }
}
