﻿using EpForceDirectedGraph.cs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class NodeDataCustom : NodeData {
    public const int NodeType_Normal = 0;
    public const int NodeType_Link = 1;
    int nodeType;
    

    public NodeDataCustom():base() {

    }

    public NodeDataCustom(string label, int nodeType):base() {
        this.nodeType = nodeType;
        this.label = label;
    }

    public int NodeType {
        get {
            return nodeType;
        }

        set {
            nodeType = value;
        }
    }
}
