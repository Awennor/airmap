﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GenericPropertySetterByEvent : MonoBehaviour {

    [SerializeField]
    StringProperty[] stringProperties;
    [SerializeField]
    IntProperty[] intProperties;
    [SerializeField]
    BoolProperty[] boolProperties;



    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void SetBoolProperty(string label, bool value) {
        foreach (var s in boolProperties) {
            if (s.label.Equals(label)) {
                s.boolEvent.Invoke(value);
            }
        }
    }

    public void SetStringProperty(string label, string value) {
        foreach (StringProperty s in stringProperties) {
            if (s.label.Equals(label)) {
                s.stringEvent.Invoke(value);
            }
        }
    }

    public void SetIntProperty(string label, int value, bool updateOnlyIfChange = false) {
        foreach (IntProperty s in intProperties) {
            if (s.label.Equals(label)) {
                if (updateOnlyIfChange && s.currentValue == value) {
                    return;
                }
                s.currentValue = value;
                s.OnPropertySet.Invoke(value);
                
                foreach(var c in s.callbacksOnValue) {
                    if(c.value == value) {
                        c.e.Invoke();
                    }
                }
            }
        }
    }

    [Serializable]
    private class StringProperty {
        public string label;
        public MyStringEvent stringEvent;

    }

    [Serializable]
    private class BoolProperty {
        public string label;
        public MyBoolEvent boolEvent;

    }

    [Serializable]
    private class IntProperty {
        public string label;
        public int currentValue;
        public MyIntEvent OnPropertySet;
        public IntProperty_Callback[] callbacksOnValue;

    }

    [Serializable]
    private class IntProperty_Callback {
        public int value;
        public UnityEvent e;
    }

}
