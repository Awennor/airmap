﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperimentIntegration : MonoBehaviour {

    public DiagnosisCommons diagnosisCommons;
    ExperimentExecuter executer = new ExperimentExecuter();

	// Use this for initialization
	void Start () {
        diagnosisCommons.OnDiagnosedCSV += DiagnosisCommons_OnDiagnosed;
        diagnosisCommons.OnDiagnosedFinal += DiagnosisCommons_OnDiagnosed;
    }

    private void DiagnosisCommons_OnDiagnosed()
    {
        executer.NextStep();
    }

    private void DiagnosisCommons_OnDiagnosed(string obj)
    {
        DiagnosisCommons_OnDiagnosed();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
