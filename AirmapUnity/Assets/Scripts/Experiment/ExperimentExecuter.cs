﻿ using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static TimeUpSupport;

public class ExperimentExecuter
{

    public void StartExperiment()
    {
        ProcessStep();
    }

    private void ProcessStep()
    {
        ExperimentProgress progress = GenericDataHolder.Instance.GetObject<ExperimentProgress>();
        if (progress == null) return;
        int p = progress.Progress;
        
        var descriptor = GenericDataHolder.Instance.GetObject<ExperimentDescriptor>();

        if (p >= descriptor.Units.Length) {
            SceneManager.LoadScene("ExperimentEnd");
            return;
        }
            
        var unit = descriptor.Units[p];
       
        int arg = unit.Argument;
        TimeUpData timeUpData = GenericDataHolder.Instance.GetObject<TimeUpData>();

        timeUpData.time = unit.TimeAlotted;
        switch (unit.ExperimentUnitType)
        {
            case ExperimentUnitType.AIRMAP:
                GenericDataHolder.Instance.Register(descriptor.Airmaps[arg]);
                SceneManager.LoadScene("AirMap");
                break;
            case ExperimentUnitType.KITBUILD:
                GenericDataHolder.Instance.Register(descriptor.Kitbuilds[arg]);
                SceneManager.LoadScene("KitBuild");
                break;
            case ExperimentUnitType.EXAM:
                GenericDataHolder.Instance.Register(descriptor.ExamData[arg]);
                SceneManager.LoadScene("Exam");
                break;
            case ExperimentUnitType.TEXT:
                GenericDataHolder.Instance.Register(descriptor.TextData[arg]);
                SceneManager.LoadScene("Text");
                break;
            default:
                break;
        }

    }

    public void NextStep()
    {
        ExperimentProgress p = GenericDataHolder.Instance.GetObject<ExperimentProgress>();
        if (p == null) return;
        p.Progress++;

        ProcessStep();
    }

}
