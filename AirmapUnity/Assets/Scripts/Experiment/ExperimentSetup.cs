﻿using Assets.Scripts.Exam;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static TimeUpSupport;

public class ExperimentSetup : MonoBehaviour {

    public ExperimentDescriptor experimentDescriptor;
    ExperimentExecuter experimentExecuter = new ExperimentExecuter();
    public bool SetupOnStart;

	// Use this for initialization
	void Start ()
    {
        Application.runInBackground = true;
        if(SetupOnStart)
            StartSetup();

    }

    public void StartSetup()
    {
        GenericDataHolder.Instance.Register(experimentDescriptor);
        ExperimentProgress experimentProgress = new ExperimentProgress();
        experimentProgress.UserName = UnityEngine.Random.Range(0, 999999) + DateTime.Now.ToString().Replace('/', '-');
        GenericDataHolder.Instance.Register(experimentProgress);
        GenericDataHolder.Instance.Register(new TimeUpData());
        experimentExecuter.StartExperiment();
    }

    // Update is called once per frame
    void Update () {
		
	}

    
    
}

public class ExperimentProgress
{

    int progress;
    string userName;

    public int Progress
    {
        get
        {
            return progress;
        }

        set
        {
            progress = value;
        }
    }

    public string UserName
    {
        get
        {
            return userName;
        }

        set
        {
            userName = value;
        }
    }
}

[Serializable]
public struct ExperimentDescriptor
{
    [SerializeField]
    string experimentName;
    [SerializeField]
    ExperimentUnit[] units;
    [SerializeField]
    KitbuildConfiguration[] kitbuilds;
    [SerializeField]
    AirmapConfiguration[] airmaps;
    [SerializeField]
    List<ExamData> examData;
    [SerializeField]
    List<TextReadController.TextReadDescriptor> textData;

    public ExperimentUnit[] Units
    {
        get
        {
            return units;
        }

        set
        {
            units = value;
        }
    }

    public KitbuildConfiguration[] Kitbuilds
    {
        get
        {
            return kitbuilds;
        }

        set
        {
            kitbuilds = value;
        }
    }

    public AirmapConfiguration[] Airmaps
    {
        get
        {
            return airmaps;
        }

        set
        {
            airmaps = value;
        }
    }

    public string ExperimentName
    {
        get
        {
            return experimentName;
        }

        set
        {
            experimentName = value;
        }
    }

    public List<ExamData> ExamData
    {
        get
        {
            return examData;
        }

        set
        {
            examData = value;
        }
    }

    public List<TextReadController.TextReadDescriptor> TextData
    {
        get
        {
            return textData;
        }

        set
        {
            textData = value;
        }
    }
}
[Serializable]
public struct ExperimentUnit
{
    [SerializeField]
    ExperimentUnitType experimentUnitType;
    [SerializeField]
    int argument;
    [SerializeField]
    float timeAlotted;

    public ExperimentUnitType ExperimentUnitType
    {
        get
        {
            return experimentUnitType;
        }

        set
        {
            experimentUnitType = value;
        }
    }

    public int Argument
    {
        get
        {
            return argument;
        }

        set
        {
            argument = value;
        }
    }

    public float TimeAlotted
    {
        get
        {
            return timeAlotted;
        }

        set
        {
            timeAlotted = value;
        }
    }
}
[Serializable]
public enum ExperimentUnitType
{
    AIRMAP, KITBUILD, EXAM, TEXT
}
[Serializable]
public struct KitbuildConfiguration
{
    [SerializeField]
    TextAsset jsonmap;
    [SerializeField]
    public bool skipFeedback;

    public TextAsset Jsonmap
    {
        get
        {
            return jsonmap;
        }

        set
        {
            jsonmap = value;
        }
    }
}
[Serializable]
public struct AirmapConfiguration
{
    [SerializeField]
    TextAsset jsonmap;
    [SerializeField]
    bool hidableLinks;

    public TextAsset Jsonmap
    {
        get
        {
            return jsonmap;
        }

        set
        {
            jsonmap = value;
        }
    }

    public bool HidableLinks
    {
        get
        {
            return hidableLinks;
        }

        set
        {
            hidableLinks = value;
        }
    }
}
