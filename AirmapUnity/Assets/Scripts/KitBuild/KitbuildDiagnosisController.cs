﻿using Assets.Scripts.ConceptMap.model;
using pidroh.conceptmap.kitbuild;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitbuildDiagnosisController : MonoBehaviour
{

    public KitBuildModel model;
    public KitBuildController control;
    ConceptMapDiagnosis diagnosis;
    public DiagnosisCommons diagnosisCommons;
    public bool generateCSV;
    public bool feedbackSkip = false;

    public void Diagnosis_ForceFinal()
    {
        diagnosis = diagnosisCommons.DiagnosisData;
        var targetMap = control.TargetMap;
        var builtMap = model.DataAccessor.Data;
        this.diagnosis.BuildDiagnosis(builtMap, targetMap);
        diagnosisCommons.DiagnoseAndLogFinal_mapOnly();
    }

    [ContextMenu("Diagnose")]
    public void Diagnosis()
    {
        if (feedbackSkip)
        {
            Diagnosis_ForceFinal();
            return;
        }
        diagnosis = diagnosisCommons.DiagnosisData;
        var targetMap = control.TargetMap;
        var builtMap = model.DataAccessor.Data;
        this.diagnosis.BuildDiagnosis(builtMap, targetMap);
        diagnosisCommons.DiagnosisToLogIntermediary();
        string diagCSV = this.diagnosis.GenerateCSV();
        if (generateCSV)
        {
            diagnosisCommons.Diagnosed(diagCSV);
        }
        else
        {
            diagnosisCommons.DiagnosedIntermediary_MapOnly(builtMap);
        }

        Debug.Log(diagCSV);
    }



}
