﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using pidroh.conceptmap.model;
using pidroh.conceptmap.kitbuild;

public class KitBuildApplication : MonoBehaviour {

    public KitBuildController controller;
    public TextAsset mapJson;
    public KitbuildDiagnosisController kbDiagnosis;
    public ConfirmationUICompletition confirmUI;

    private void Awake()
    {
        //Debug.Log(mapJson.text);

        var config = GenericDataHolder.Instance.GetObject<KitbuildConfiguration>();
        if (config.Jsonmap != null)
        {
            mapJson = config.Jsonmap;
            Debug.Log("Json map set");
            kbDiagnosis.feedbackSkip = config.skipFeedback;
            if (kbDiagnosis.feedbackSkip)
            {
                confirmUI.skipUI = false;
            }
        }


        ConceptMapData map = ConceptMapDataUtils.LoadFromJson(mapJson.text);
        Debug.Log("map title "+map.Title);
        controller.TargetMap = map;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
