﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace pidroh.conceptmap.kitbuild.view
{
    public class KitBuildView : MonoBehaviour
    {
        [SerializeField]
        private NodeManager nodeManager;
        [SerializeField]
        private LinkManager linkManager;
        public event Action<ConnectionInfoUI> OnConnected;
        public DragDropManager dragDropManager = new DragDropManager();
        List<NodeDraggableMetaData> nodeDragMeta = new List<NodeDraggableMetaData>();
        List<LinkDraggableMetaData> linkConnection_DragMeta = new List<LinkDraggableMetaData>();
        List<ConnectionInfoUI> activeConnections = new List<ConnectionInfoUI>();
        List<ConnectionInfoUI> notEnforcedConnections = new List<ConnectionInfoUI>();
        [SerializeField]
        public bool automaticallyConnect = true;
        public bool debug;
        public event Action<int> OnNodeDragStart;
        public event Action<int> OnNodeClickStart;
        public event Action<int> OnLinkDragStart;
        public event Action<int> OnLinkClickStart;
        public event Action<int, int> OnLinkConnectorDragStart;
        public event Action<int> OnNodeDragEnd;
        public event Action<int> OnLinkDragEnd;
        public event Action<int, int> OnLinkConnectorDragEnd;
        public event Action<int, int> OnLinkConnectorClickStart;

        public RectTransform backgroundLayer;

        public NodeManager NodeManager
        {
            get
            {
                return nodeManager;
            }

            set
            {
                nodeManager = value;
            }
        }

        public LinkManager LinkManager
        {
            get
            {
                return linkManager;
            }

            set
            {
                linkManager = value;
            }
        }

        public void SetNodeName(int nodeId, string name)
        {
            NodeManager.SetNodeName(nodeId, name);
        }
        public void SetLinkName(int linkId, string name)
        {
            LinkManager.SetLinkName(linkId, name);


        }
        public void RequestLinksAndNodes(int nNodes, int nLinks)
        {
            LinkManager.LinkAmount(nLinks);
            NodeManager.NodeAmount(nNodes);
        }

        public void Update()
        {
            LinkManager.SetAllConnectorCirclesVisible(true);
            for (int i = 0; i < activeConnections.Count; i++)
            {
                var c = activeConnections[i];

                if (notEnforcedConnections.Contains(c))
                {

                }
                else
                {
                    var link = c.Link;
                    var node = c.Node;
                    for (int j = 0; j < linkConnection_DragMeta.Count; j++)
                    {
                        var m = linkConnection_DragMeta[j];
                        if (m.LinkId == link && m.Connector == c.Connector)
                        {
                            var drag = m.Drag;
                            drag.transform.position = nodeDragMeta[node].D.transform.position;
                            LinkManager.ChangeConnectorParent(c.Link, c.Connector, backgroundLayer);
                            LinkManager.SetConnectorCircleVisible(c.Link, c.Connector, false);
                        }
                    }
                }


            }
        }

        internal void ResetPositions()
        {
            NodeManager.InitialPositions();
            LinkManager.InitialPositions();
        }

        public void Awake()
        {
            dragDropManager.OnOverlap += DragDropManager_OnClosestOverlappingDecided;
            dragDropManager.CheckOverlapStart += DragDropManager_CheckOverlapStart;
            dragDropManager.OnConnectionDesire += DragDropManager_OnConnectionDesire;
            dragDropManager.OnNoConnectionDesire += DragDropManager_OnNoConnectionDesire;
            NodeManager.OnDraggableCreated += NodeManager_OnDraggableCreated;
            LinkManager.OnDraggableLinkCreated += LinkManager_OnDraggableLinkCreated;
            LinkManager.OnDraggableConnectorCreated += LinkManager_OnDraggableConnectorCreated;
            LinkManager.OnLinkClickStart += LinkManager_OnLinkClickStart;
            LinkManager.OnLinkClickEnd += LinkManager_OnLinkClickEnd;
            dragDropManager.CheckConnectionsStart += DragDropManager_CheckConnectionsStart;
        }



        private void DragDropManager_CheckConnectionsStart()
        {
            if (debug)
                Debug.Log("KitbuildView: check connection start");
            //activeConnections.Clear();
            //notEnforcedConnections.Clear();
        }

        private void LinkManager_OnLinkClickEnd(int link)
        {
            for (int i = 0; i < activeConnections.Count; i++)
            {
                if (activeConnections[i].Link == link)
                {
                    LinkManager.ToOriginalConnectorParent(link, activeConnections[i].Connector);
                    notEnforcedConnections.Add(activeConnections[i]);
                    //activeConnections.RemoveAt(i);
                    //i--;
                    dragDropManager.CheckOverlappingSpots();

                }
            }

        }



        private void LinkManager_OnLinkClickStart(int link)
        {
            notEnforcedConnections.Clear();
            //dragDropManager.AttemptFormConnections();
        }

        private void DragDropManager_OnNoConnectionDesire(Draggable obj)
        {
            LinkDraggableMetaData linkMeta = (LinkDraggableMetaData)obj.MetaData;
            var conInfo = new ConnectionInfoUI(-1, linkMeta.LinkId, linkMeta.Connector);
            bool broke = BreakAllConnectionsOfSameLinkAndConnector(conInfo);
            if (broke)
            {
                if (OnConnected != null)
                {
                    OnConnected(conInfo);
                }
            }

            //if (AlreadyConnected(conInfo))
            //{
            //}
        }

        private void DragDropManager_OnConnectionDesire(RectTransform arg1, Draggable arg2)
        {
            var meta = arg1.GetComponent<Draggable>().MetaData;
            NodeDraggableMetaData nodeMeta = (NodeDraggableMetaData)meta;
            LinkDraggableMetaData linkMeta = (LinkDraggableMetaData)arg2.MetaData;
            ConnectionInfoUI connectionInfo = new ConnectionInfoUI(nodeMeta.NodeId, linkMeta.LinkId, linkMeta.Connector);
            if (AlreadyConnected(connectionInfo))
            {

            }
            else
            {
                if (debug)
                    Debug.Log("Connection Desire");
                BreakAllConnectionsOfSameLinkAndConnector(connectionInfo);
                if (OnConnected != null)
                {

                    OnConnected(connectionInfo);
                }

                if (automaticallyConnect)
                {
                    if (debug)
                        Debug.Log("Connection!!");
                    activeConnections.Add(connectionInfo);
                    dragDropManager.CheckOverlappingSpots();
                }
            }

        }

        private bool BreakAllConnectionsOfSameLinkAndConnector(ConnectionInfoUI connectionInfo)
        {
            bool brokeConnection = false;
            for (int i = 0; i < activeConnections.Count; i++)
            {
                var c = activeConnections[i];
                if (c.Link == connectionInfo.Link && c.Connector == connectionInfo.Connector)
                {
                    brokeConnection = true;
                    activeConnections.RemoveAt(i);
                    notEnforcedConnections.Remove(c);
                    i--;

                }
            }

            return brokeConnection;
        }

        private bool AlreadyConnected(ConnectionInfoUI connectionInfo)
        {
            for (int i = 0; i < activeConnections.Count; i++)
            {
                ConnectionInfoUI c = activeConnections[i];

                if (c.EquivalentConnection(connectionInfo))
                {
                    return true;
                }
            }
            return false;
        }

        private void LinkManager_OnDraggableConnectorCreated(Draggable arg1, int linkId, int connectorId)
        {
            int link = linkId;
            LinkDraggableMetaData linkDraggableMetaData = new LinkDraggableMetaData(linkId, connectorId, arg1);
            linkConnection_DragMeta.Add(linkDraggableMetaData);

            dragDropManager.RegisterDraggable(arg1);
            arg1.MetaData = linkDraggableMetaData;
            arg1.OnStartDrag += (d) =>
            {
                if (OnLinkConnectorDragStart != null)
                    OnLinkConnectorDragStart(link, connectorId);
            };
            arg1.OnEndDrag += (d) =>
            {
                if (OnLinkConnectorDragEnd != null)
                    OnLinkConnectorDragEnd(link, connectorId);
                //dragDropManager.AttemptFormConnections();
            };

            arg1.OnPointerDown += (d) =>
            {
                if (OnLinkConnectorClickStart != null)
                {
                    OnLinkConnectorClickStart(link, connectorId);
                }
            };
        }

        private void LinkManager_OnDraggableLinkCreated(Draggable arg1, int linkId)
        {
            int link = linkId;

            arg1.OnMove += (d) =>
            {
                notEnforcedConnections.Clear();
                NodeManager.UnSelectAllNodes();
            };
            arg1.OnStartDrag += (d) =>
            {
                if (OnLinkDragStart != null)
                    OnLinkDragStart(link);
            };
            arg1.OnEndDrag += (d) =>
            {
                if (OnLinkDragEnd != null)
                    OnLinkDragEnd(link);
                //dragDropManager.AttemptFormConnections();
            };
            arg1.OnPointerDown += (d) =>
            {
                if (OnLinkClickStart != null) {
                    OnLinkClickStart(link);
                }
            };
        }

        private void NodeManager_OnDraggableCreated(Draggable arg1, int nodeId)
        {
            NodeDraggableMetaData nodeDraggableMetaData = new NodeDraggableMetaData(nodeId, arg1);
            nodeDragMeta.Add(nodeDraggableMetaData);

            dragDropManager.RegisterSpot(arg1.GetComponent<RectTransform>());
            arg1.MetaData = nodeDraggableMetaData;

            arg1.OnPointerDown += (d) =>
            {
                if (OnNodeClickStart != null)
                {
                    OnNodeClickStart(nodeId);
                }
            };

            arg1.OnStartDrag += (d) =>
            {
                if (OnNodeDragStart != null)
                    OnNodeDragStart(nodeId);
            };
            arg1.OnEndDrag += (d) =>
            {
                if (OnNodeDragEnd != null)
                    OnNodeDragEnd(nodeId);
                //dragDropManager.AttemptFormConnections();
            };
            arg1.OnMove += (d) =>
            {
                notEnforcedConnections.Clear();
                NodeManager.UnSelectAllNodes();
                //dragDropManager.CheckOverlappingSpots();
                //dragDropManager.AttemptFormConnections();
            };

        }


        private void DragDropManager_CheckOverlapStart()
        {
            NodeManager.UnSelectAllNodes();
        }

        private void DragDropManager_OnClosestOverlappingDecided(RectTransform arg1, Draggable arg2)
        {
            if (arg1 != null)
            {
                var meta = arg1.GetComponent<Draggable>().MetaData;
                NodeDraggableMetaData nodeMeta = (NodeDraggableMetaData)meta;
                var linkM = (LinkDraggableMetaData)arg2.MetaData;
                ConnectionInfoUI connectionInfoUI = new ConnectionInfoUI(node: nodeMeta.NodeId, link: linkM.LinkId, connector: linkM.Connector);

                bool alreadyConnected = AlreadyConnected(connectionInfoUI);
                bool enforced = isEnforced(connectionInfoUI);
                if (!alreadyConnected || !enforced)
                {
                    NodeManager.SelectNode(nodeMeta.NodeId);
                }

                if (debug)
                {
                    Debug.Log("Kitbuild: Closest Overlap " + nodeMeta.NodeId);
                }
            }
            else
            {
            }
        }

        private bool isEnforced(ConnectionInfoUI connectionInfoUI)
        {
            for (int i = 0; i < notEnforcedConnections.Count; i++)
            {
                if (connectionInfoUI.EquivalentConnection(notEnforcedConnections[i]))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
