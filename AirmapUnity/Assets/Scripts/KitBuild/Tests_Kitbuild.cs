﻿using pidroh.conceptmap.kitbuild.view;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tests_Kitbuild : MonoBehaviour {

    public LinkManager linkManager;
    public NodeManager nodeManager;

    [ContextMenu("CreateLinks")]
    public void CreateLinks() {
        linkManager.LinkAmount(5);
    }

    [ContextMenu("CreateOneLink")]
    public void CreateLink()
    {
        linkManager.LinkAmount(1);
    }

    [ContextMenu("CreateOneNode")]
    public void CreateNode()
    {
        nodeManager.NodeAmount(1);
    }

    [ContextMenu("CreateNodes")]
    public void CreateNodes()
    {
        nodeManager.NodeAmount(5);
    }

}
