﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class LinkUIKB : MonoBehaviour, IPointerClickHandler, IPointerDownHandler {

    [SerializeField]
    private Button button;
    [SerializeField]
    private Text text;
    [SerializeField]
    private GameObject[] connectors;
    [SerializeField]
    private GameObject[] connectorCircles;
    [SerializeField]
    private RectTransform[] lines;
    public event Action<LinkUIKB> OnCompleteClick;
    public event Action<LinkUIKB> OnStartClick;

    public Button Button
    {
        get
        {
            return button;
        }

        set
        {
            button = value;
        }
    }

    public GameObject[] Connectors
    {
        get
        {
            return connectors;
        }

        set
        {
            connectors = value;
        }
    }

    public RectTransform[] Lines
    {
        get
        {
            return lines;
        }

        set
        {
            lines = value;
        }
    }

    public Text Text
    {
        get
        {
            return text;
        }

        set
        {
            text = value;
        }
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        if (OnStartClick != null) {
            OnStartClick(this);
        }
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        //Debug.Log("click happen!");
        if (OnCompleteClick != null) {
            OnCompleteClick(this);
        }
    }

    internal void SetConnectorVisible(bool visible, int connector)
    {
        connectorCircles[connector].SetActive(visible);

    }
}
