﻿namespace pidroh.conceptmap.kitbuild.view
{
    public struct LinkDraggableMetaData
    {
        int linkId;
        int connector;
        Draggable drag;

        public LinkDraggableMetaData(int linkId, int connector, Draggable drag)
        {
            this.linkId = linkId;
            this.connector = connector;
            this.drag = drag;
        }

        public int LinkId
        {
            get
            {
                return linkId;
            }

            set
            {
                linkId = value;
            }
        }

        public int Connector
        {
            get
            {
                return connector;
            }

            set
            {
                connector = value;
            }
        }

        public Draggable Drag
        {
            get
            {
                return drag;
            }

            set
            {
                drag = value;
            }
        }
    }
}
