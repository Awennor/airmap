﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace pidroh.conceptmap.kitbuild.view
{
    public class LinkManager : MonoBehaviour
    {

        public LinkUIKB link_prefab;
        [SerializeField]
        private List<LinkUIKB> links = new List<LinkUIKB>();
        public GameObject linkParent;
        public event Action<Draggable, int, int> OnDraggableConnectorCreated;
        public event Action<Draggable, int> OnDraggableLinkCreated;
        public event Action<int> OnLinkClickStart;
        public event Action<int> OnLinkClickEnd;
        public GameObject initialSpawnPosition;
        public Vector3 initialSpawnPositionDistance;
        List<int> aux = new List<int>();

        public List<LinkUIKB> Links
        {
            get
            {
                return links;
            }

            set
            {
                links = value;
            }
        }

        public void LinkAmount(int amount) {
            
            while (Links.Count < amount)
            {
                var linkNew = Instantiate(link_prefab);
                linkNew.OnCompleteClick += LinkNew_OnCompleteClick;
                linkNew.OnStartClick += LinkNew_OnStartClick;
                var draggable = linkNew.gameObject.AddComponent<Draggable>();
                linkNew.transform.SetParent(linkParent.transform);
                Links.Add(linkNew);
                linkNew.transform.position = new Vector3(10,10,0);

                if (OnDraggableLinkCreated != null) {
                    OnDraggableLinkCreated(draggable, Links.Count-1);
                }

                float disOff = 80;
                Vector3 offset = new Vector3(-disOff, 0);
                int connector = 0;
                SetupConnector(linkNew, offset, connector);

                offset = new Vector3(disOff, 0);
                connector = 1;
                SetupConnector(linkNew, offset, connector);
                linkNew.transform.localScale = Vector3.one;
            }
            InitialPositions();
        }

        internal void SetAllConnectorCirclesVisible(bool visible)
        {
            for (int i = 0; i < Links.Count; i++)
            {
                Links[i].SetConnectorVisible(visible, 0);
                Links[i].SetConnectorVisible(visible, 1);
            }
        }

        internal void SetLinkName(int linkId, string name)
        {
            Links[linkId].Text.text = name;
        }

        public void InitialPositions()
        {
            var elements = this.Links;
            Vector3 pos = initialSpawnPosition.transform.position;
            NodeManager.InitialPositions(initialSpawnPosition, elements, aux, initialSpawnPositionDistance);

        }

        private void LinkNew_OnStartClick(LinkUIKB obj)
        {
            if (OnLinkClickStart != null)
                OnLinkClickStart(Links.IndexOf(obj));
        }

        internal void SetConnectorCircleVisible(int link, int connector, bool visible)
        {
            Links[link].SetConnectorVisible(visible, connector);
        }

        private void LinkNew_OnCompleteClick(LinkUIKB obj)
        {
            if (OnLinkClickEnd != null) OnLinkClickEnd.Invoke(Links.IndexOf(obj));
        }

        

        private void SetupConnector(LinkUIKB linkNew, Vector3 offset, int connector)
        {
            GameObject con = linkNew.Connectors[connector];
            con.transform.position = linkNew.transform.position + offset;
            Draggable draggable = con.gameObject.AddComponent<Draggable>();
            if(OnDraggableConnectorCreated != null)
                OnDraggableConnectorCreated(draggable, Links.IndexOf(linkNew), connector);

        }

        public void LateUpdate()
        {
            FixLines();
        }

        public void FixLines() {
            for (int i = 0; i < Links.Count; i++)
            {
                var connectors = Links[i].Connectors;
                var linkNew = Links[i];
                for (int j = 0; j < 2; j++)
                {
                    var con = connectors[j];
                    int connector = j;
                    FixConnectorLine(linkNew, con, connector);
                }
            }
            
        }

        internal void ToOriginalConnectorParent(int link, int connector)
        {
            Links[link].Connectors[connector].transform.parent = linkParent.transform;
            Links[link].Connectors[connector].transform.SetAsFirstSibling();
        }

        internal void ChangeConnectorParent(int link, int connector, RectTransform backgroundLayer)
        {
            Links[link].Connectors[connector].transform.SetParent(backgroundLayer);
        }

        private static void FixConnectorLine(LinkUIKB linkNew, GameObject con, int connector)
        {
            var arg2 = linkNew.transform.position;
            var arg3 = con.transform.position;
            var dis = (arg3 - arg2);
            var lineTransform = linkNew.Lines[connector];
            lineTransform.rotation = Quaternion.Euler(0, 0, Mathf.Atan(dis.y / dis.x) * Mathf.Rad2Deg);
            lineTransform.position = arg2 + dis / 2;
            float scaleX = dis.magnitude;
            var lineRectT = lineTransform.GetComponent<RectTransform>();
            lineRectT.sizeDelta = new Vector2(scaleX, 2);
        }
    }

}