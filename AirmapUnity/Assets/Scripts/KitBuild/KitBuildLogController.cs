﻿using pidroh.conceptmap.kitbuild;
using pidroh.conceptmap.kitbuild.view;
using Pidroh.UnityUtils.LogDataSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;

public class KitBuildLogController : MonoBehaviour
{
    public const string label_MapType = "maptype";
    public const string label_MapStart = "mapstart";
    private const string label_Connection = "connection";
    public const string label_Proposition = "proposition";
    private const string label_NodeP = "position-node";
    private const string label_linkP = "position-link";
    public LogCapturerManager logSystem;
    public KitBuildApplication kitbuildApplication;
    public KitBuildView kitbuildView;
    public KitBuildController kbControl;
    StringBuilder stringB = new StringBuilder();

    // Use this for initialization
    void Start()
    {
        kbControl.OnMapStart += KbControl_OnMapStart;
        kitbuildView.OnConnected += KitbuildView_OnConnected;
        kitbuildView.OnNodeDragStart += KitbuildView_OnNodeDragStart;
        kitbuildView.OnNodeDragEnd += KitbuildView_OnNodeDragEnd;
        kitbuildView.OnLinkConnectorDragEnd += KitbuildView_OnLinkConnectorDragEnd;
        kitbuildView.OnLinkConnectorDragStart += KitbuildView_OnLinkConnectorDragStart;
        kitbuildView.OnLinkDragEnd += KitbuildView_OnLinkDragEnd;
        kitbuildView.OnLinkDragStart += KitbuildView_OnLinkDragStart;
        kitbuildView.OnLinkClickStart += KitbuildView_OnLinkClickStart;
        kitbuildView.OnNodeClickStart += KitbuildView_OnNodeClickStart;
        kitbuildView.OnLinkConnectorClickStart += KitbuildView_OnLinkConnectorClickStart;

    }

    private void KitbuildView_OnLinkConnectorClickStart(int arg1, int arg2)
    {
        ClickEvent("linkc", arg1, arg2);
    }



    private void KitbuildView_OnNodeClickStart(int obj)
    {
        ClickEvent("node", obj);
    }

    private void KitbuildView_OnLinkClickStart(int obj)
    {
        ClickEvent("link", obj);
    }

    private void ClickEvent(string additionalLabel, int arg1, int arg2 = -1)
    {
        if(arg2 < 0)
            logSystem.DirectLog("clickstart", additionalLabel, arg1+"");
        else
            logSystem.DirectLog("clickstart", additionalLabel, arg1 + "", arg2+"");
    }

    private void KitbuildView_OnLinkDragStart(int obj)
    {
        DragEvent("dragstart", "link", obj);
    }

    private void KitbuildView_OnLinkDragEnd(int obj)
    {
        DragEvent("dragend", "link", obj);
    }

    private void KitbuildView_OnLinkConnectorDragStart(int arg1, int arg2)
    {
        DragEvent("dragstart", "linkc", arg1, arg2);
    }

    private void KitbuildView_OnLinkConnectorDragEnd(int arg1, int arg2)
    {
        DragEvent("dragend", "linkc", arg1, arg2);
    }

    private void KitbuildView_OnNodeDragEnd(int node)
    {
        DragEvent("dragend", "node", node);
    }

    private void KitbuildView_OnNodeDragStart(int node)
    {
        DragEvent("dragstart", "node", node);
    }

    private void DragEvent(string eventLabl, string target, int id)
    {
        PositionLogEvents();
        logSystem.DirectLog(eventLabl, target + "", id + "");
    }

    private void DragEvent(string eventLabl, string target, int id, int connector)
    {
        PositionLogEvents();
        logSystem.DirectLog(eventLabl, target + "", id + "", connector+"");
    }

    private void KbControl_OnMapStart(string obj)
    {
        logSystem.DirectLog(label_MapStart, "kitbuild");
        logSystem.DirectLog(label_MapStart, obj);
    }

    private void KitbuildView_OnConnected(ConnectionInfoUI obj)
    {
        PositionLogEvents();
        logSystem.DirectLog(label_Connection, obj.Node+"", obj.Link+"", obj.Connector+"");
    }

    private void PositionLogEvents()
    {
        var linkManager = kitbuildView.LinkManager;
        var nodeManager = kitbuildView.NodeManager;
        {
            stringB.Clear();
            var items = nodeManager.Nodes;
            for (int i = 0; i < items.Count; i++)
            {
                stringB.Append(i);
                stringB.Append(',');
                stringB.Append(items[i].transform.position.x);
                stringB.Append(',');
                stringB.Append(items[i].transform.position.y);
                if (i != items.Count - 1)
                    stringB.Append(',');
                
            }
            logSystem.DirectLog(label_NodeP, stringB.ToString());
        }
        {
            stringB.Clear();
            var items = linkManager.Links;
            for (int i = 0; i < items.Count; i++)
            {
                stringB.Append(i);
                stringB.Append(',');
                stringB.Append(items[i].transform.position.x);
                stringB.Append(',');
                stringB.Append(items[i].transform.position.y);
                stringB.Append(',');
                stringB.Append(items[i].Connectors[0].transform.position.x);
                stringB.Append(',');
                stringB.Append(items[i].Connectors[0].transform.position.y);
                stringB.Append(',');
                stringB.Append(items[i].Connectors[1].transform.position.x);
                stringB.Append(',');
                stringB.Append(items[i].Connectors[1].transform.position.y);
                if (i != items.Count - 1)
                    stringB.Append(',');
            }
            logSystem.DirectLog(label_linkP, stringB.ToString());
        }
        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
