﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace pidroh.conceptmap.kitbuild.view
{
    [Serializable]
    public class DragDropManager
    {
        List<RectTransform> dragSpots = new List<RectTransform>();
        List<RectTransform> aux = new List<RectTransform>();
        List<Draggable> draggables = new List<Draggable>();
        public event Action CheckOverlapStart;
        public event Action CheckConnectionsStart;
        public event Action<RectTransform, Draggable> OnOverlap;
        public event Action<RectTransform, Draggable> OnConnectionDesire;
        public event Action<Draggable> OnNoConnectionDesire;
        public bool debug;

        public void RegisterDraggable(Draggable d)
        {
            d.OnMove += D_OnMove;
            d.OnEndDrag += D_OnEndDrag;
            draggables.Add(d);
        }

        private void D_OnEndDrag(Draggable obj)
        {
            if (debug)
                Debug.Log("DragDropManager: End Drag");
            AttemptFormConnections();
            //if (closestOverlapping != null) {

            //    if (OnConnectionDesire != null) {
            //        if(debug)
            //            Debug.Log("DragDropManager: connection desire here");
            //        OnConnectionDesire(closestOverlapping, obj);
            //    }

            //}
        }

        public void AttemptFormConnections()
        {
            if (CheckConnectionsStart != null)
            {
                CheckConnectionsStart();
            }
            if (debug)
            {
                Debug.Log("attempt connect 0");
            }
            for (int i = 0; i < draggables.Count; i++)
            {

                var drag = draggables[i];
                var closestOverlap = GetClosestOverlappingSpot(drag);
                if (debug)
                {
                    Debug.Log("attempt connect");
                }
                if (closestOverlap != null)
                {
                    if (debug)
                    {
                        Debug.Log("attempt connect 2");
                    }
                    OnConnectionDesire(closestOverlap, drag);
                }
                else
                {
                    OnNoConnectionDesire(drag);
                }
            }
        }

        private void D_OnMove(Draggable obj)
        {
            //todo change this to use checkoverlappingspots
            CheckOverlappingSpots();
            //var lastClosest = this.closestOverlapping;
            //RectTransform closestOverlapping = GetClosestOverlappingSpot(obj);
            //this.closestOverlapping = closestOverlapping;
            //if (OnOverlap != null && lastClosest != closestOverlapping)
            //{
            //    OnOverlap(closestOverlapping, obj);
            //}
        }

        public void CheckOverlappingSpots()
        {
            if (CheckOverlapStart != null)
            {
                CheckOverlapStart();
            }
            for (int i = 0; i < draggables.Count; i++)
            {
                var rect = GetClosestOverlappingSpot(draggables[i]);
                if (OnOverlap != null && rect != null)
                {
                    //Debug.Log("ON OVERLAP");
                    OnOverlap(rect, draggables[i]);
                }

            }
        }

        private RectTransform GetClosestOverlappingSpot(Draggable obj)
        {

            RectTransform rectTransform = obj.GetComponent<RectTransform>();
            Rect rect1 = GetRect(rectTransform, debug);

            RectTransform closestOverlapping = null;

            float shortestDistance = 99999999;
            for (int i = 0; i < dragSpots.Count; i++)
            {
                var spot = dragSpots[i];
                var rect2 = GetRect(spot, debug);
                if (rect1.Overlaps(rect2))
                {
                    var dis = rect2.position - rect1.position;
                    float sqrMagnitude = dis.sqrMagnitude;
                    if (debug)
                    {
                        Debug.Log("DragDropManager: Calculating Distance " + i + " " + sqrMagnitude);
                        Debug.Log("DragDropManager: Shortest Distance Now " + shortestDistance);
                    }
                    if (sqrMagnitude < shortestDistance)
                    {
                        shortestDistance = sqrMagnitude;
                        closestOverlapping = spot;
                    }
                }
            }

            return closestOverlapping;
        }

        private static Rect GetRect(RectTransform rectTransform, bool debug)
        {
            var rect1 = rectTransform.rect;
            //if(debug)
            //    Debug.Log(rect1+"b");
            rect1.x += rectTransform.position.x;
            rect1.y += rectTransform.position.y;
            //if(debug)
            //    Debug.Log(rect1 + "a");
            return rect1;
        }

        internal void RegisterSpot(RectTransform rect)
        {
            dragSpots.Add(rect);
        }
    }
}