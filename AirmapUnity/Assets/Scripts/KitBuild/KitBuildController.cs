﻿using pidroh.conceptmap.kitbuild.view;
using pidroh.conceptmap.model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using pidroh.conceptmap.

namespace pidroh.conceptmap.kitbuild
{
    public class KitBuildController : MonoBehaviour
    {

        public KitBuildModel model;
        public KitBuildView view;
        ConceptMapData targetMap;
        public event Action<string> OnMapStart;
        

        public ConceptMapData TargetMap
        {
            get
            {
                return targetMap;
            }

            set
            {
                targetMap = value;
            }
        }

        // Use this for initialization
        void Start()
        {
            if (OnMapStart != null)
                OnMapStart(targetMap.Title);
            List<string> nodeNames = targetMap.NodeNames;
            Debug.Log(nodeNames.Count);
            model.DataAccessor.AddNodes(nodeNames);
            List<string> linkNames = targetMap.LinkNames;
            model.DataAccessor.AddLinks(linkNames);
            model.DataAccessor.CreateOnePropositionForEachLink();
            view.RequestLinksAndNodes(targetMap.NodeNames.Count, targetMap.LinkNames.Count);
            for (int i = 0; i < nodeNames.Count; i++)
            {
                view.SetNodeName(i,nodeNames[i]);
            }
            for (int i = 0; i < linkNames.Count; i++)
            {
                view.SetLinkName(i, linkNames[i]);
            }
            view.OnConnected += View_OnConnected;
            Canvas.ForceUpdateCanvases();
            view.ResetPositions();
        }

        private void View_OnConnected(ConnectionInfoUI obj)
        {
            int nodeId = obj.Node;
            int linkId = obj.Link;
            int whichNode = obj.Connector;
            model.DataAccessor.SetPropositionInfo(nodeId, linkId, whichNode);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}