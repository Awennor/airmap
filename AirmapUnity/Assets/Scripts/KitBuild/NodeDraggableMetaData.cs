﻿namespace pidroh.conceptmap.kitbuild.view
{
    public struct NodeDraggableMetaData {
        int nodeId;
        Draggable d;

        public NodeDraggableMetaData(int nodeId, Draggable d)
        {
            this.nodeId = nodeId;
            this.d = d;
        }

        public int NodeId
        {
            get
            {
                return nodeId;
            }

            set
            {
                nodeId = value;
            }
        }

        public Draggable D
        {
            get
            {
                return d;
            }

            set
            {
                d = value;
            }
        }
    }
}
