﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeManager : MonoBehaviour {

    public NodeUIKB nodePrefab;
    [SerializeField]
    private List<NodeUIKB> nodes;
    public GameObject nodeParent;
    public event Action<Draggable, int> OnDraggableCreated;
    public Color nodeSelectedColor;
    public Color nodeUnselectedColor;
    private int latestSelectedNode = -1;
    public GameObject initialSpawnPosition;
    public Vector3 initialSpawnPositionDistance;
    List<int> aux = new List<int>();

    public List<NodeUIKB> Nodes
    {
        get
        {
            return nodes;
        }

        set
        {
            nodes = value;
        }
    }

    public void NodeAmount(int amount)
    {
        while (Nodes.Count < amount)
        {
            int nodeId = Nodes.Count;
            var nodeNew = Instantiate(nodePrefab);
            var drag = nodeNew.gameObject.AddComponent<Draggable>();
            if (OnDraggableCreated != null)
                OnDraggableCreated(drag, nodeId);
            nodeNew.transform.SetParent(nodeParent.transform);
            nodeNew.transform.position = new Vector3(10, 10, 0);
            Nodes.Add(nodeNew);
            nodeNew.transform.localScale = Vector3.one;
        }
        InitialPositions();
    }

    internal void SetNodeName(int nodeId, string name)
    {
        Nodes[nodeId].Text.text = name;
    }

    public static void Shuffle<T>(IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = UnityEngine.Random.Range(0, n+1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public void InitialPositions()
    {
        var initialSpawnPosition = this.initialSpawnPosition;
        var elements = this.Nodes;
        var aux = this.aux;
        InitialPositions(initialSpawnPosition, elements, aux, initialSpawnPositionDistance);
    }

    public static void InitialPositions<T>(GameObject initialSpawnPosition, List<T> elements, List<int> aux,Vector3 initialSpawnPositionDistance) where T: MonoBehaviour
    {
        Vector3 pos = initialSpawnPosition.transform.localPosition;
        aux.Clear();
        for (int i = 0; i < elements.Count; i++)
        {
            aux.Add(i);
        }
        Shuffle(aux);
        for (int i = 0; i < elements.Count; i++)
        {
            Debug.Log(aux[i] + " random");
        }
        for (int i = 0; i < elements.Count; i++)
        //for (int i = elements.Count-1; i >= 0; i--)
        {
            var e = elements[aux[i]];
            //NodeUIKB e = elements[i];
            float height = e.GetComponent<RectTransform>().rect.height;
            pos.y -= height / 2;
            e.transform.localPosition = pos;
            pos += initialSpawnPositionDistance;
            pos.y -= height / 2;
        }
    }

    internal void SelectNode(int nodeId)
    {
        if (nodeId >= 0)
        {
            latestSelectedNode = nodeId;
            var back = Nodes[nodeId].Background;
            back.color = nodeSelectedColor;
        }

    }

    internal void SelectNodeAndDeselectLatest(int nodeId)
    {
        if (nodeId >= 0)
        {
            if (latestSelectedNode != nodeId && latestSelectedNode != -1) {
                DeselectLatest();
            }
            latestSelectedNode = nodeId;
            var back = Nodes[nodeId].Background;
            back.color = nodeSelectedColor;
        }
        else {
            if (latestSelectedNode >= 0)
            {
                DeselectLatest();

            }
            latestSelectedNode = -1;
        }
    }

    private void DeselectLatest()
    {
        var back = Nodes[latestSelectedNode].Background;
        back.color = nodeUnselectedColor;
    }

    internal void UnSelectAllNodes()
    {
        for (int i = 0; i < Nodes.Count; i++)
        {
            var back = Nodes[i].Background;
            back.color = nodeUnselectedColor;
        }
    }
}
