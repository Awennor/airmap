﻿using System;

namespace pidroh.conceptmap.kitbuild.view
{
    public struct ConnectionInfoUI {
        int node;
        int link;
        int connector;
        

        public ConnectionInfoUI(int node, int link, int connector)
        {
            this.node = node;
            this.link = link;
            this.connector = connector;
        }

        public int Node
        {
            get
            {
                return node;
            }

          
        }

        public int Link
        {
            get
            {
                return link;
            }


        }

        public int Connector
        {
            get
            {
                return connector;
            }

        }

        internal bool EquivalentConnection(ConnectionInfoUI c)
        {
            return c.connector == this.connector && c.link == this.link && c.node == this.node;
        }
    }
}
