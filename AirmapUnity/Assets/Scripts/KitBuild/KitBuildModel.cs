﻿using Assets.Scripts.ConceptMap.model;
using pidroh.conceptmap.model;
using System;
using System.Collections;
using System.Collections.Generic;

namespace pidroh.conceptmap.kitbuild
{

    public class KitBuildModel : UnityEngine.MonoBehaviour
    {
        ConceptMapDataAccessor dataAccessor = new ConceptMapDataAccessor(new ConceptMapData());

        internal ConceptMapDataAccessor DataAccessor
        {
            get
            {
                return dataAccessor;
            }

            set
            {
                dataAccessor = value;
            }
        }
    }
}