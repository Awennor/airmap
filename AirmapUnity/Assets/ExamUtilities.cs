﻿using Assets.Scripts.Exam;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class ExamUtilities : MonoBehaviour {

    
    [Serializable]
    private class ExamUtilityData {
        public string examName;
        public string[] questions;
        public TextAsset questionTextAsset;
    }
    [SerializeField]
    ExamUtilityData[] datas;

    public ExperimentSetup[] experimentSetups;
    
    [ContextMenu("ProcessData")]
    public void ProcessData() {
        foreach (var experimentSetup in experimentSetups)
        {
            var exams = experimentSetup.experimentDescriptor.ExamData;
            exams.Clear();

            foreach (var d in datas)
            {
                ExamData eD = new ExamData();
                eD.ExamLabel = d.examName;
                string[] questions = d.questions;
                AddQuestions(eD, questions);
                if (d.questionTextAsset != null)
                {
                    AddQuestions(eD, d.questionTextAsset.text.Split(new string[] { "$$" }, StringSplitOptions.RemoveEmptyEntries));
                }
                exams.Add(eD);
                Debug.Log("" + eD.ExamLabel);
            }
            Debug.Log("DONE " + exams.Count);
        }
        

    }

    private static void AddQuestions(ExamData eD, string[] questions)
    {
        for (int i = 0; i < questions.Length; i++)
        {
            MultipleAnswerQuestion maq = new MultipleAnswerQuestion();
            string q = questions[i];
            var lines = q.Split('$');
            maq.Question = lines[0];
            maq.Id = i;
            eD.QuestionIds.Add(i);
            for (int j = 1; j < lines.Length; j++)
            {
                maq.Items.Add(lines[j]);
            }
            eD.Questions.AddQuestion(maq);
        }
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
