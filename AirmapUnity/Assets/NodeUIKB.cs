﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NodeUIKB : MonoBehaviour {

    [SerializeField]
    private Image background;
    [SerializeField]
    private Text text;

    public Image Background
    {
        get
        {
            return background;
        }

        set
        {
            background = value;
        }
    }

    public Text Text
    {
        get
        {
            return text;
        }

        set
        {
            text = value;
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
