﻿komodo dragon|also known as|komodo monitor
komodo dragon|can grow to |maximum of 3 meters
maximum of 3 meters| in | rare cases
komodo dragon|has|large size
large size|attributed to|Island gigantism
large size|attributed to|representative of extinct large varanid lizards
representative of extinct large varanid lizards|according to|recent research
komodo dragon|is|ecosystem dominator
ecosystem dominator|attributed to|large size
komodo dragon|has|toxic protein
toxic protein|secreted from|lower jaw
komodo dragon|mates|between May and August
komodo dragon|lays|20 eggs
20 eggs|in|September
20 eggs|in|a nesting hole
20 eggs|hatch in|April
April|is|when insects are most plentiful