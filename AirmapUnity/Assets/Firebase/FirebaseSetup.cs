﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirebaseSetup : MonoBehaviour {

    public TextAsset textAsset;

    public void Awake()
    {
        if (textAsset != null) {
            var json = textAsset.text;
            var config = JsonUtility.FromJson<FirebaseConfig>(json);
            
            FirebaseSingleton.Instance.Init(config);
        }
        
    }

}
