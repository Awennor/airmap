﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirebaseSingleton
{

    static FirebaseSingleton instance;

    FirebaseConnectionManager connection;

    public static FirebaseSingleton Instance
    {
        get
        {
            if (instance == null) instance = new FirebaseSingleton();
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public FirebaseConnectionManager Connection
    {
        get
        {
            return connection;
        }

        set
        {
            connection = value;
        }
    }

    internal bool IsInit()
    {
        return Connection != null;
    }

    public void Init(FirebaseConfig config) {
        Connection = new FirebaseConnectionManager(config.Secret, config.Url);
    }

    internal void Save(string path, string data)
    {
        Connection.SaveInfo(data,path);
    }
}

[Serializable]
public class FirebaseConfig {
    [SerializeField]
    string secret;
    [SerializeField]
    string url;

    public string Secret
    {
        get
        {
            return secret;
        }

        set
        {
            secret = value;
        }
    }

    public string Url
    {
        get
        {
            return url;
        }

        set
        {
            url = value;
        }
    }
}
