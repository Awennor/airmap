﻿using SimpleFirebaseUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FirebaseTest : MonoBehaviour {
    private Firebase firebase;
    UriBuilder uribuilder = new UriBuilder();

    [ContextMenu("test")]
    public void TestFirebase() {
        uribuilder.Scheme = "https";
        uribuilder.Host = "secretmachine-fb281.firebaseio.com";
        uribuilder = new UriBuilder("https", "secretmachine-fb281.firebaseio.com");
        if (firebase == null)
            firebase = Firebase.CreateNew("secretmachine-fb281.firebaseio.com", "DAijFiMTAvTAxeLOkc5bPN78PC2ZQfT7i5oAF2OH");
        Debug.Log(firebase.Host+"___Firebase host");
        Debug.Log(uribuilder.Uri.AbsolutePath);
        uribuilder.Path = "/test";
        
        firebase.OnSetSuccess += OnSetSuccess;
        firebase.OnSetFailed += OnSetFail;
        firebase.Child("blablu", true).SetValue("{\"alanisawesome\": {\"name\": \"Alan Turing\", \"birthday\": \"June 23, 1912\"}}");
        

    }

    private void OnSetFail(Firebase arg1, FirebaseError arg2) {
        Debug.Log("SET FAIL!!!" + arg2.Message + arg2.InnerException);
    }

    private void OnSetSuccess(Firebase arg1, DataSnapshot arg2) {
        Debug.Log("SET SUCCESS!!!");
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
