﻿using Pidroh.UnityUtils.LogDataSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class FirebaseMapIntegration : MonoBehaviour
{

    public LogCapturerManager logCapturer;
    public DiagnosisCommons diagnosis;
    private string path;
    private string diagnosisLog;

    // Use this for initialization
    void Awake()
    {
        if (FirebaseSingleton.Instance.IsInit())
        {
            diagnosis.OnDiagnosedCSV += Diagnosis_OnDiagnosed;
            diagnosis.OnDiagnosedFinal += Diagnosis_OnDiagnosed;
        }

    }

    private void Diagnosis_OnDiagnosed()
    {
        Diagnosis_OnDiagnosed(null);
    }

    private void Diagnosis_OnDiagnosed(string csvDiagnosis)
    {
        this.diagnosisLog = csvDiagnosis;
        var exp = GenericDataHolder.Instance.GetObject<ExperimentDescriptor>();

        var progress = GenericDataHolder.Instance.GetObject<ExperimentProgress>();
        if (progress == null) return;
        //get log data and convert to cvs
        var logs = logCapturer.LogDatas;
        string logcvs = LogConverter.ToCSV(logs, '$');

        string username = progress.UserName;

        var expName = exp.ExperimentName;
        int p = progress.Progress;
        path = expName + "/" + "Step " + p + " " + exp.Units[p].ExperimentUnitType + "/" + username + "/" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture) + "/";
        Debug.Log("blu");
        //FirebaseSingleton.Instance.Save("bla", "blu");

        FirebaseSingleton.Instance.Save(path + "log", logcvs);

        if (diagnosisLog != null)
        {
            FirebaseSingleton.Instance.Save(path + "diagnosis", diagnosisLog);
        }



    }


    // Update is called once per frame
    void Update()
    {

    }
}
