﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class ParticlePlayable : BasicPlayableBehaviour
{
	public ExposedReference<ParticleSystem> Particles;

	public bool StartEnabled;
	public bool EndEnabled;
    public Color ParticleColour = Color.white;

	private ParticleSystem _particles;
	private ParticleSystem.EmissionModule _particlesEmission;

	public override void OnGraphStart(Playable playable) 
	{
		_particles = Particles.Resolve (playable.GetGraph().GetResolver());

		if (_particles == null)
			return;
		
		_particlesEmission = _particles.emission;
	}

	public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        if (_particles == null)
			return;
		
		_particlesEmission.enabled = StartEnabled;

		ParticleSystem.MainModule mainModule = _particles.main;
		ParticleSystem.MinMaxGradient colourGradient = new ParticleSystem.MinMaxGradient(ParticleColour);
		mainModule.startColor = colourGradient;
    }
	
    public override void OnBehaviourPause(Playable playable, FrameData info)
	{
        if (_particles == null)
			return;
			
        _particlesEmission.enabled = EndEnabled;
	}
}
