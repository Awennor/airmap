﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class TimelineStartPlayable : BasicPlayableBehaviour {

    public ExposedReference<PlayableDirector> playDirector;
    private PlayableDirector _director;

    public override void OnGraphStart(Playable playable) 
	{
        _director = playDirector.Resolve(playable.GetGraph().GetResolver());
	}

    public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        _director.Stop();
        _director.Play();
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
