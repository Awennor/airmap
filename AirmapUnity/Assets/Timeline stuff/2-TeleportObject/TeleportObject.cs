﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class TeleportObject : BasicPlayableBehaviour
{
    public ExposedReference<GameObject> ThingToMove;
	public ExposedReference<Transform> StartTransform;
	public ExposedReference<Transform> EndTransform;

    private GameObject _thingToMove;
    private Transform _startTransform;
    private Transform _endTransform;

	public override void OnGraphStart(Playable playable) 
	{
        _thingToMove = ThingToMove.Resolve(playable.GetGraph().GetResolver());
		_startTransform = StartTransform.Resolve(playable.GetGraph().GetResolver());
		_endTransform = EndTransform.Resolve(playable.GetGraph().GetResolver());
	}

    public override void OnBehaviourPlay(Playable playable, FrameData info) 
	{
		if (_startTransform != null) 
		{
			MoveObject(_startTransform);
		}
	}
    public override void OnBehaviourPause(Playable playable, FrameData info) 
	{
		if ( _endTransform != null) 
		{
			MoveObject(_endTransform);
		}	
	}

	private void MoveObject(Transform targetTransform)
	{
        _thingToMove.transform.position = targetTransform.position;
        _thingToMove.transform.rotation = targetTransform.rotation;
	}
}
