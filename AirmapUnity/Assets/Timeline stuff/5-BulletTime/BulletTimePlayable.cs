﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class BulletTimePlayable : BasicPlayableBehaviour
{
	public float BulletTimeTimeScale;

	private float _originalTimeScale = 1f;

	public override void ProcessFrame(Playable playable, FrameData info, object playerData) 
	{
        //This checks if we're actually playing. Prevents it running before the clip has started.
		if (playable.GetTime() <= 0)
			return;
		
		Time.timeScale = Mathf.Lerp (_originalTimeScale, BulletTimeTimeScale, (float)(playable.GetTime() / playable.GetDuration()));
	}
	public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        _originalTimeScale = Time.timeScale;
    }
}