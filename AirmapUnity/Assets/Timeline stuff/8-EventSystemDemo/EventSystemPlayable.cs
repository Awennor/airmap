﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.EventSystems;
using UnityEngine.AI;
using UnityEngine.Timeline;

public class EventSystemPlayable : BasicPlayableBehaviour
{
	public ExposedReference<EventTrigger> TimelineEventTrigger;

	private EventTrigger _eventTrigger;

	private bool _alreadyTriggered;

	public override void OnGraphStart(Playable playable)
	{
		_eventTrigger = TimelineEventTrigger.Resolve (playable.GetGraph().GetResolver());
	}

	public override void OnBehaviourPlay(Playable playable, FrameData info) 
	{
		if (!_alreadyTriggered && _eventTrigger != null) 
		{
			_eventTrigger.OnSubmit (null);
			_alreadyTriggered = true;
		}
	}
}
