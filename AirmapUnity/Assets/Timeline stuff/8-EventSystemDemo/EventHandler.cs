﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventHandler : MonoBehaviour 
{
    public void DoSomething()
    {
        Debug.Log("Event handler method called");
    }
}
