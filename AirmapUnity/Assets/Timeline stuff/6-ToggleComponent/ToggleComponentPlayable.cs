﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class ToggleComponentPlayable : BasicPlayableBehaviour
{
	public ExposedReference<MonoBehaviour> Component;

	private MonoBehaviour _component;

	public bool EnabledOnStart;
	public bool EnabledOnEnd;

	private GameObject m_GameObject;

	// called when the owning graph starts playing
	public override void OnGraphStart(Playable playable) 
	{
		_component = Component.Resolve(playable.GetGraph().GetResolver());
	}	
	
	public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        if (_component != null) 
		{
			_component.enabled = EnabledOnStart;
		}
    }
	
    public override void OnBehaviourPause(Playable playable, FrameData info)
	{
		if (_component != null) 
		{
			_component.enabled = EnabledOnEnd;
		}	
	}
}
