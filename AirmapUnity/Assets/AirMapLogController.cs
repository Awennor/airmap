﻿using Pidroh.UnityUtils.LogDataSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;

public class AirMapLogController : MonoBehaviour
{

    public GraphTest graphTest;
    public AirmapApplication airmapApp;
    public LogCapturerManager logSystem;
    public AirMapController airmapController;
    public AirMapModel model;
    StringBuilder sb = new StringBuilder();
    public ConnectionChoserUIManager connection;
    // Use this for initialization
    void Start()
    {
        logSystem.DirectLog(KitBuildLogController.label_MapType, "airmap");
        var title = airmapApp.Map.Title;
        logSystem.DirectLog(KitBuildLogController.label_MapStart, title);
        logSystem.DirectLog("Hidable links", connection.HidableLinksLastState+"");
        airmapController.OnPropositionMade += AirmapController_OnPropositionMade;
        airmapController.OnNodeSelectionStatusChange += AirmapController_OnNodeSelectionStatusChange;
        graphTest.OnConnected += GraphTest_OnConnected;
        graphTest.OnDisconnected += GraphTest_OnDisconnected;
        graphTest.OnNodeClick += GraphTest_OnNodeClick;
        graphTest.OnNodeClickEnd += GraphTest_OnNodeClickEnd;
        graphTest.OnLinkDestroy.AddListener(GraphTest_OnLinkDestroy);
        connection.OnLinkButtonClicked += Connection_OnLinkButtonClicked;
        
    }

    private void GraphTest_OnDisconnected(pidroh.conceptmap.airmap.view.ViewStructures.PropositionMade obj)
    {
        logSystem.DirectLog("proposition_broken_ui_ids",
            obj.Node1UIId, obj.Node2UIId, obj.LinkUIId);
    }

    private void GraphTest_OnLinkDestroy(string arg0)
    {
        logSystem.DirectLog("link_destroy", model.GetLinkId(arg0)+"");
    }

    private void Connection_OnLinkButtonClicked(string obj)
    {
        int v = model.GetLinkId(obj);
        logSystem.DirectLog("link_button_clicked", v + "");
    }

    private void GraphTest_OnNodeClickEnd(string obj)
    {
        logSystem.DirectLog("node_click_end_UI",
           obj);
    }

    private void GraphTest_OnNodeClick(string obj)
    {
        logSystem.DirectLog("node_click_UI",
           obj);
    }

    private void GraphTest_OnConnected(pidroh.conceptmap.airmap.view.ViewStructures.PropositionMade obj)
    {
        logSystem.DirectLog("proposition_ui_ids",
            obj.Node1UIId, obj.Node2UIId, obj.LinkUIId);
    }

    private void AirmapController_OnNodeSelectionStatusChange(int arg1, bool arg2)
    {

        if(arg2)
            logSystem.DirectLog("node_selected", arg1+"");
        else
            logSystem.DirectLog("node_unselected", arg1 + "");
    }

    private void AirmapController_OnPropositionMade(pidroh.conceptmap.model.Proposition obj)
    {
        CapturePositions();
        logSystem.DirectLog(KitBuildLogController.label_Proposition,
            obj.Node1 + "", obj.Node2 + "", obj.Link + "");
    }

    private void CapturePositions()
    {
        var nDs = graphTest.NodeDrawers;
        var vs = nDs.Values;
        
        foreach (var nd in vs)
        {
            var type = nd.Type; //link or node
            float x = nd.transform.position.x;
            float y = nd.transform.position.y;
            sb.Append(nd.name);
            sb.Append(',');
            sb.Append(type);
            sb.Append(',');
            sb.Append(x);
            sb.Append(',');
            sb.Append(y);
            sb.Append(',');
        }
        sb.Length--; //to remove the last comma
        logSystem.DirectLog("ui_positions", sb.ToString());

    }

    // Update is called once per frame
    void Update()
    {

    }
}
