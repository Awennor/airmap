﻿using pidroh.conceptmap.model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConceptMapValidator : MonoBehaviour {

    public TextAsset textAsset;

	// Use this for initialization
    [ContextMenu("Show propositions")]
	void ShowPropositions () {
        string json = textAsset.text;
        var map = ConceptMapDataUtils.LoadFromJson(json);
        var ps = map.Propositions;
        for (int i = 0; i < ps.Count; i++)
        {
            var p = ps[i];
            List<string> nodes = map.NodeNames;
            Debug.Log(
                string.Format(
                    "Proposition {0}:\nNode1 ({1}) : {2}\nNode2 ({3}) : {4}\nLink ({5}) : {6}",
                    i, p.Node1, nodes[p.Node1], p.Node2, nodes[p.Node2], p.Link, map.LinkNames[p.Link]
                    )
                );
        }
    }

    [ContextMenu("Show propositions II")]
    void ShowPropositionsII()
    {
        string json = textAsset.text;
        var map = ConceptMapDataUtils.LoadFromJson(json);
        var ps = map.Propositions;
        string text = "";
        for (int i = 0; i < ps.Count; i++)
        {
            var p = ps[i];
            List<string> nodes = map.NodeNames;
            text +=
                string.Format(
                    "Proposition {0}: {1} {3} {2}\n",
                    i, nodes[p.Node1], nodes[p.Node2], map.LinkNames[p.Link]
                    );
        }
        Debug.Log(text);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
