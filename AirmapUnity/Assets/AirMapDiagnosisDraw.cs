﻿using System;
using System.Collections;
using System.Collections.Generic;
using pidroh.conceptmap.model;
using UnityEngine;

public class AirMapDiagnosisDraw : MonoBehaviour {

    public AirmapDiagnosis diagnosis;
    public AirMapModel model;
    public GraphTest graphTest;
    int step;

    [ContextMenu("Draw Diagnosis")]
    public void DrawDiagnosis() {
        graphTest.UpdateNodeType = false;
        
        
        diagnosis.DiagnosisPure();
        var diag = diagnosis.DiagnosisData;
        var correct = diag.Correct;
        graphTest.OnConnected += GraphTest_OnConnected;
        graphTest.DestroyAllLinks();

        step = 0;
        CreateLinks(correct);
        step = 1;
        CreateLinks(diag.Incorrect);
        step = 2;
        CreateLinks(diag.Missing);

    }

    private void GraphTest_OnConnected(pidroh.conceptmap.airmap.view.ViewStructures.PropositionMade obj)
    {
        var linkUIId = obj.LinkUIId;
        var nD = graphTest.NodeDrawers[linkUIId];
        //nD.GetComponent<GenericPropertySetterByEvent>().SetIntProperty("nodetype",2+step, true);
        nD.GetComponent<GenericPropertySetterByEvent>().SetIntProperty("nodetype", 2+step, true);
    }

    private void CreateLinks(List<Proposition> props)
    {
        var mapData = model.MapData;
        foreach (var c in props)
        {
            graphTest.CreateLink(
                mapData.NodeNames[c.Node1],
                mapData.NodeNames[c.Node2],
                mapData.LinkNames[c.Link]
                );
        }
        
    }
}
