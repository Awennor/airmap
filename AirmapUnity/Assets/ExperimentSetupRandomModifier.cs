﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperimentSetupRandomModifier : MonoBehaviour {
    [SerializeField]
    ModifierGroup[] modifierGroups;
    public ExperimentSetup setup;
    public bool startSetup = true;
    public bool randomDecideOnStart = true;
    public int forceRandom = -1;

	// Use this for initialization
	void Start ()
    {
        //randomly decide on a modifiergroup
        if(randomDecideOnStart)
            RandomDecide();
    }

    private void RandomDecide()
    {
        int random = UnityEngine.Random.Range(0, modifierGroups.Length);
        Debug.Log("Random option " + random);
        if (forceRandom >= 0)
        {
            random = forceRandom;
        }
        DecideModifierGroup(random);
    }

    public void DecideModifierGroup(int random)
    {
        Debug.Log("Modifier group " + random);
        var m = modifierGroups[random];
        foreach (var item in m.modifiers)
        {
            setup.experimentDescriptor.Units[item.step] = item.unit;
        }
        if (startSetup)
            setup.StartSetup();
    }

    // Update is called once per frame
    void Update () {
		
	}

    [Serializable]
    class ModifierGroup
    {
        public Modifier[] modifiers;
    }

    [Serializable]
    class Modifier {
        public int step;
        public ExperimentUnit unit;
    }
}
