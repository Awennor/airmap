﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteSetter : MonoBehaviour {
    public MySpriteEvent mySpriteEvent;

    public Sprite[] sprites;

    public void SetSprite(int which) {
        if(sprites.Length > which)
            mySpriteEvent.Invoke(sprites[which]);
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
