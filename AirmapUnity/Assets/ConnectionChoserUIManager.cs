﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ConnectionChoserUIManager : MonoBehaviour {

    public GenericPropertySetterByEvent connectionButtonMatrix;

    public List<GenericPropertySetterByEvent> connectionButtons;
    public GameObject connectionButtonParent;
    private List<ConnectionManager.ConnectionStatement> currentConnections;
    public MyStringEvent OnConnectionCreateChosen;
    public UnityEvent OnHideUI;
    List<int> linkAmount = new List<int>();
    public GraphTest graphTest;
    public Animator uiAnimator;
    public Toggle hidableLinks;
    public Button closeButton;

    public bool LinkShow { get; private set; }
    public bool HidableLinksLastState { get; private set; }

    public event Action<string> OnLinkButtonClicked;

    public void RequestConnectionButtons(int amount) {
        while(connectionButtons.Count < amount) {
            var cbm = Instantiate(this.connectionButtonMatrix);
            cbm.transform.SetParent(connectionButtonParent.transform);
            connectionButtons.Add(cbm);
            Button button = cbm.GetComponent<Button>();
            
            button.onClick.AddListener(()=> {
                CbmClicked(cbm);
            });

        }
        for (int i = 0; i < connectionButtons.Count; i++) {
            connectionButtons[i].gameObject.SetActive(i < amount);
        }
    }

    internal void HidableLinksState(bool hidableLinks)
    {
        this.HidableLinksLastState = hidableLinks;
        uiAnimator.enabled = hidableLinks;
        closeButton.gameObject.SetActive(hidableLinks);
    }

    public void ConnectionDestroyed_External(string label) {
        var cs = currentConnections;
        for (int i = 0; i < cs.Count; i++) {
            if(label.Equals( cs[i].Label)) {
                linkAmount[i]++;
                UpdateButtonNames();
            }
            
        }
    }

    private void CbmClicked(GenericPropertySetterByEvent cbm)
    {
        var indexOfConnection = connectionButtons.IndexOf(cbm);
        linkAmount[indexOfConnection]--;
        var connectionStatement = currentConnections[indexOfConnection];
        string labelChosen = connectionStatement.Label;
        if (OnLinkButtonClicked != null)
        {
            OnLinkButtonClicked(labelChosen);
        }
        UpdateButtonNames();
        OnConnectionCreateChosen.Invoke(labelChosen);
        HideUI();
        
    }

    private void HideUI()
    {
        OnHideUI.Invoke();
        ButtonsSetInteractable(false);
        
    }

    public void ShowConnections(List<ConnectionManager.ConnectionStatement> cs) {
        this.currentConnections = cs;
        RequestConnectionButtons(cs.Count);
        linkAmount.Clear();
        for (int i = 0; i < cs.Count; i++) {
            linkAmount.Add(cs[i].Amount);
        }
        
        UpdateButtonNames();
        ButtonsSetInteractable(false);
    }

    private void UpdateButtonNames() {
        var cs = currentConnections;

        for (int i = 0; i < cs.Count; i++) {
            var label = cs[i].Label;
            
            connectionButtons[i].SetStringProperty( "edgelabel", label + "<color=grey> (" + linkAmount[i] + ")</color>");
            connectionButtons[i].SetBoolProperty("interactable",  linkAmount[i]> 0);
            connectionButtons[i].gameObject.SetActive(linkAmount[i] > 0);
            
        }
    }

    // Use this for initialization
    void Start () {
        hidableLinks.onValueChanged.AddListener((b)=> {
            HidableLinksState(b);
        });
        graphTest.OnRequestNodeAction.AddListener(()=> {
            LinkShow = true;
            ButtonsSetInteractable(true);
        });
        graphTest.OnNotTwoNodesSelected.AddListener(()=> {
            HideUI();
        });
	}

    private void ButtonsSetInteractable(bool interactable)
    {
        if (interactable)
        {
            UpdateButtonNames();
        }
        else {
            foreach (var cbms in connectionButtons)
            {
                cbms.GetComponent<Button>().interactable = interactable;
            }
        }
        
    }


    // Update is called once per frame
    void Update () {
		
	}
}
