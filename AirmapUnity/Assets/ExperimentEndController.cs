﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class ExperimentEndController : MonoBehaviour {

    public Text codeText;
    public InputField inputCode;
    private string username;

    [DllImport("__Internal")]
    private static extern void HelloString(string str);

    // Use this for initialization
    void Start () {
        ExperimentProgress experimentProgress = GenericDataHolder.Instance.GetObject<ExperimentProgress>();

        username = "username";
        if(experimentProgress != null)
            username = experimentProgress.UserName; 
        codeText.text = username;
        inputCode.text = username;
	}

    public void ShowUserName() {
        HelloString(username);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
