﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class ExperimentSetupModifierURL : MonoBehaviour {

    public string[] urlPart;
    public int[] modifierGroup;
    public ExperimentSetupRandomModifier experimentSetupModifier;
    int defaultGroup = 0;
    public string desktopDocTitle;
    [DllImport("__Internal")]
    private static extern string GetTopURL();
    [DllImport("__Internal")]
    private static extern string GetDocTitle();


    // Use this for initialization
    void Awake () {
        if (!enabled) return;
        string url = desktopDocTitle;
        if (Application.platform == RuntimePlatform.WebGLPlayer)
            url = GetDocTitle();
        for (int i = 0; i < urlPart.Length; i++)
        {
            string item = urlPart[i];
            if (url.Contains(item))
            {
                experimentSetupModifier.DecideModifierGroup(modifierGroup[i]);
                break;

            }
        }
        experimentSetupModifier.DecideModifierGroup(defaultGroup);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
