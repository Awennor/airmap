﻿{"NodeNames":[
		"the dogsled race",
		"Alaska",
		"Cold weather",
		"Julie's dogs",
		"A competition",

		"running",
		"team that works together",
		"participant",
		"one of the coldest places on earth",
		"thick fur coat"
	],
	"LinkNames":[
		"took place in",
		"has",
		"have a",
		"protects from the",
		"pulled the sled slowly before",

		"are a",
		"are a",
		"in",
		"is",
		"is"
	],

	"Propositions":[
		{"Node1":0,"Node2":1,"Link":0},
		{"Node1":1,"Node2":2,"Link":1},
		{"Node1":3,"Node2":9,"Link":2},
		{"Node1":2,"Node2":9,"Link":3},
		{"Node1":3,"Node2":5,"Link":4},

		{"Node1":3,"Node2":7,"Link":5},
		{"Node1":3,"Node2":6,"Link":6},
		{"Node1":7,"Node2":0,"Link":7},
		{"Node1":1,"Node2":8,"Link":8},
		{"Node1":4,"Node2":0,"Link":9}
	]}