﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerEvents : MonoBehaviour {

    public MyVector3Event OnPointerDown;
    public MyVector3Event OnPointerUp;
    public MyVector3Event OnPointerMove;
    public MyVector3Event OnPointerNotMove;
    private bool dragging;
    private Vector3 oldPos;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        var mousePosition = Input.mousePosition;
        mousePosition.z = 2;
        var positionConv = Camera.main.ScreenToWorldPoint(mousePosition);
        positionConv.z = 0;
        if (Input.GetMouseButtonUp(0)) {
            dragging = false;
            OnPointerUp.Invoke(positionConv);
            //Debug.Log("Clear Drag buffer");
        }
        if (Input.GetMouseButtonDown(0)) {
            dragging = true;
            OnPointerDown.Invoke(positionConv);
            //Debug.Log("Clear Drag buffer");
        }
        if (dragging) {
            //Debug.Log("MOVE");
            
             if (oldPos == positionConv) {
                //Debug.Log("NOT MOVE");
                OnPointerNotMove.Invoke(positionConv);
            } else {
                OnPointerMove.Invoke(positionConv);
            }
            //Debug.Log("Clear Drag buffer");
        } else {
           

        }
        oldPos = positionConv;

    }
}
