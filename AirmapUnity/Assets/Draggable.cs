﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler {
    private Vector3 startPosition;
    private Vector3 startMouse;
    object metaData;
    

    public event Action<Draggable> OnMove;
    public event Action<Draggable> OnEndDrag;
    public event Action<Draggable> OnStartDrag;
    public event Action<Draggable> OnPointerDown;

    public object MetaData
    {
        get
        {
            return metaData;
        }

        set
        {
            metaData = value;
        }
    }

    //[SerializeField]
    //private Transform transform;
    

    public void OnDrag() {
        if(!enabled) return;
        Vector3 mousePosition = Input.mousePosition;
        
        //mousePosition = Camera.main.WorldToScreenPoint(mousePosition);

        transform.position = mousePosition - startMouse + startPosition;
        if(OnMove != null)
            OnMove(this);
    }

    public void StartDrag() {
        if(!enabled) return;
        if (OnStartDrag != null)
            OnStartDrag(this);
        startPosition = transform.position;
        Vector3 mousePosition = Input.mousePosition;
        
        //mousePosition = Camera.main.WorldToScreenPoint(mousePosition);
        startMouse = mousePosition;
    }

	// Use this for initialization
	void Start () {
        //var ev = gameObject.AddComponent<EventTrigger>();
        //ev.
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnBeginDrag(PointerEventData eventData) {
        StartDrag();
    }

    public void OnDrag(PointerEventData eventData) {
        OnDrag();
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData)
    {
        //Debug.Log("end drag");
        if (OnEndDrag != null)
        {
            OnEndDrag(this);
        }
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        OnPointerDown?.Invoke(this);
    }
}
