﻿using Pidroh.UnityUtils.LogDataSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class TimeUpSupport : MonoBehaviour
{

    public LogCapturerManager log;
    [SerializeField]
    float timeLeft;
    public UnityEvent OnTimeUp;
    private DateTime timePrevious;

    private void TimeUp()
    {
        log.DirectLog("TimeUp");
        OnTimeUp.Invoke();
    }

    // Use this for initialization
    void Start()
    {
        TimeUpData timeUpData = GenericDataHolder.Instance.GetObject<TimeUpData>();
        if (timeUpData != null)
            timeLeft = timeUpData.time;
        timePrevious = DateTime.Now;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeLeft > 0)
        {
            var sa = DateTime.Now - timePrevious;
            float deltaTime = sa.Milliseconds / 1000f;
            timeLeft -= deltaTime;
            if (timeLeft <= 0)
            {
                TimeUp();
            }
        }
        timePrevious = DateTime.Now;
    }

    public class TimeUpData
    {
        public float time = -1;
    }


}
