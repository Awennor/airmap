﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultipleAnswerUI : MonoBehaviour {

    public Text questionText;
    public ToggleGroup toggleGroup;
    public List<ToggleComponent> toggles;
    public ToggleComponent prefab;
    public Transform toggleParent;
    public event Action<int, MultipleAnswerUI> OnQuestionAnswered;

    public void NumberOfItems(int number) {
        while (toggles.Count < number) {
            ToggleComponent toggleComponent = Instantiate(prefab);
            toggleComponent.gameObject.SetActive(true);
            toggleComponent.OnCheckChange += ToggleComponent_OnCheckChange;
            toggleComponent.transform.SetParent(toggleParent);
            toggles.Add(toggleComponent);
        }
    }

    private void ToggleComponent_OnCheckChange(ToggleComponent obj)
    {
        //int index = toggles.IndexOf(obj);
        for (int i = 0; i < toggles.Count; i++)
        {
            if (toggles[i].Toggle.isOn) {
                if (OnQuestionAnswered != null)
                {
                    OnQuestionAnswered(i, this);
                }
                return;
            }
        }
        

    }

    // Use this for initialization
    void Start () {
        prefab.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    internal void SetItems(List<string> items)
    {
        NumberOfItems(items.Count);
        for (int i = 0; i < toggles.Count; i++)
        {
            toggles[i].Label.text = items[i];
        }
    }
}
