﻿using pidroh.conceptmap.model;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMakingUtil : MonoBehaviour
{

    public TextAsset sketch;
    public string resultJson;

    // Use this for initialization
    [ContextMenu("Create map")]
    void Create()
    {
        ConceptMapData mapData = new ConceptMapData();
        var text = sketch.text;
        text = text.Replace("\r", "");
        var lines = text.Split('\n');

        foreach (var l in lines)
        {
            var parts = l.Split('|');
            for (int i = 0; i < parts.Length; i++)
            {
                parts[i] = parts[i].Trim();
            }
            int node1 = TryCreateNode(parts[0], mapData);
            int node2 = TryCreateNode(parts[2], mapData);
            mapData.LinkNames.Add(parts[1]);
            int link = mapData.LinkNames.Count - 1;
            mapData.Propositions.Add(new Proposition(node1, node2, link));
        }
        mapData.Title = sketch.name;
        var json = ConceptMapDataUtils.SaveToJson(mapData);
        resultJson = json;
        //Debug.Log(json);
    }

    private int TryCreateNode(string nodeName, ConceptMapData mapData)
    {
        if (mapData.NodeNames.Contains(nodeName))
        {

        }
        else {
            mapData.NodeNames.Add(nodeName);
        }
        return mapData.NodeNames.IndexOf(nodeName);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
