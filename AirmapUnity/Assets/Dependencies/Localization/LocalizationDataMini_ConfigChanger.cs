﻿using Pidroh.UnityUtils.LocalizationMini;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizationDataMini_ConfigChanger : MonoBehaviour {

    public LocalizationDataMiniConfig config;
    public int language;

	// Use this for initialization
	void Awake () {
        config.ChosenLanguage = language;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
