﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Pidroh.UnityUtils.LocalizationMini
{
    public class LocalizationMiniTextConversionUtil : EditorWindow
    {

        [MenuItem("Utilities/LocalizationMini/TextConversion")]

        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(LocalizationMiniTextConversionUtil));
        }

        string text = "TEXT SEPARATED BY $$";
        string localiName = "File name";
        LocalizationDataMiniConfig config;
        bool trim;

        void OnGUI()
        {

            //assetName = EditorGUILayout.TextField("S. Asset Name:", assetName);

            //target = EditorGUILayout.ObjectField("Label:", target, typeof(GameObject), true) as GameObject;
            text = EditorGUILayout.TextArea(text);
            localiName = EditorGUILayout.TextField(localiName);
            trim = EditorGUILayout.Toggle("trim", trim);
            Type type = typeof(LocalizationDataMiniConfig);
            var o = EditorGUILayout.ObjectField(config, type, false);
            if (o != null)
            {
                config = o as LocalizationDataMiniConfig;
            }



            if (GUILayout.Button("Create assets"))
            {

                string path = "Assets";
                foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
                {
                    path = AssetDatabase.GetAssetPath(obj);
                    if (File.Exists(path))
                    {
                        path = Path.GetDirectoryName(path);
                    }
                    break;
                }

                //fieldN.Split(new[] { "s" });
                var values = text.Split(new[] { "$$" }, StringSplitOptions.None);
                int assetId = 0;
                for (int i = 0; i < values.Length; i++)
                {
                    var v = values[i];
                    if (trim)
                    {
                        v = v.Trim();
                    }

                    if (v.Trim().Length > 0)
                    {
                        assetId++;
                        var loc = ScriptableObject.CreateInstance<LocalizationDataMini>();
                        loc.Values = new[] { v };
                        string pathNow = path + "/" + localiName + assetId + ".asset";
                        AssetDatabase.CreateAsset(loc, pathNow);

                    }

                }

                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                EditorUtility.FocusProjectWindow();

            }

        }



    }
}