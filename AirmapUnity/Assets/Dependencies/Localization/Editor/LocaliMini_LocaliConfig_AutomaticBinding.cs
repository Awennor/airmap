﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Pidroh.UnityUtils.LocalizationMini
{
    public class LocaliMini_LocaliConfig_AutomaticBinding : EditorWindow
    {

        [MenuItem("Utilities/LocalizationMini/Config_AutomaticBinding")]

        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(LocaliMini_LocaliConfig_AutomaticBinding));
        }

        LocalizationDataMiniConfig config;


        void OnGUI()
        {

            //assetName = EditorGUILayout.TextField("S. Asset Name:", assetName);

            //target = EditorGUILayout.ObjectField("Label:", target, typeof(GameObject), true) as GameObject;
            Type type = typeof(LocalizationDataMiniConfig);
            var o = EditorGUILayout.ObjectField(config, type, false);
            if (o != null)
            {
                config = o as LocalizationDataMiniConfig;
            }

            string path = "Assets";
            foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
            {
                path = AssetDatabase.GetAssetPath(obj);
                if (File.Exists(path))
                {
                    
                    path = Path.GetDirectoryName(path);
                }
                break;
            }
            EditorGUILayout.LabelField(path+"/");



            if (GUILayout.Button("Bind all"))
            {

                var assetsFound = AssetDatabase.FindAssets("t:LocalizationDataMini", new []{path});
                int amount = 0;
                foreach (var guid in assetsFound)
                {
                    var lms = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GUIDToAssetPath(guid));
                    
                    foreach (var obj in lms)
                    {
                        var lm = obj as LocalizationDataMini;
                        if (lm != null) {
                            amount++;
                            lm.Config = config;
                            EditorUtility.SetDirty(lm);
                            
                        }
                    }
                }
                Debug.Log("Bound "+amount +" localiminis _ AutomaticBinding utility");

                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

            }

        }



    }
}