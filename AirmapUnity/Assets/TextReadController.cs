﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TextReadController : MonoBehaviour
{

    public Text text;
    public Text title;
    public UnityEvent OnReadDone;
    private TextReadDescriptor descriptor;

    public TextReadDescriptor Descriptor
    {
        get
        {
            return descriptor;
        }

        set
        {
            descriptor = value;
        }
    }

    public void ReadDone() {
        OnReadDone.Invoke();
    }

    // Use this for initialization
    void Awake()
    {
        Descriptor = GenericDataHolder.Instance.GetObject<TextReadDescriptor>();
        if (Descriptor != null)
        {
            title.text = descriptor.title;
            text.text = Descriptor.content.text;
        }
        else {
            text.text = "BBLASIDJOIASDJSAIODJSAIODJOISADSADsAddd\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\ndsadsd";
        }
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    [Serializable]
    public class TextReadDescriptor
    {
        public string title;
        public TextAsset content;
    }
}
